import { combineReducers, Reducer } from "redux";

import { editor } from "./pages/Editor/reducer";
import { createUser } from "./pages/CreateUser/reducer";
import { specEditor } from "./pages/SpecEditor/reducer";
import { trace } from "./pages/Trace/reducer";
import { logins } from "./pages/MultiLogin/reducer";
import { produce } from "./pages/Create/reducer";
import { sales } from "./pages/Sales/reducer";
import { watchlist } from "./pages/Watchlist/reducer";

export interface RootState {
  [x: string]: any;
}

export default combineReducers<RootState>({
  editor,
  createUser,
  specEditor,
  trace,
  logins,
  produce,
  sales,
  watchlist
});
