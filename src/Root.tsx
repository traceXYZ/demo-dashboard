import React, { Component } from "react";
import { Provider } from "react-redux";
import { Router } from "react-router";
import { Store, AnyAction } from "redux";
import { History } from "history";

interface RootProps {
  store: Store<any, AnyAction>;
  history: History;
  routes: Node | JSX.Element;
}

export default class Root extends Component<RootProps, any> {
  render() {
    const { store, history, routes } = this.props;

    return (
      <Provider store={store}>
        {<Router history={history}>{routes}</Router>}
      </Provider>
    );
  }
}
