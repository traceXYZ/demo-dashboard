import { all } from "redux-saga/effects";

import { watchSendInvokeAsync, watchSendQueryAsync } from "./pages/Editor/saga";
import { watchCreateUserAsync } from "./pages/CreateUser/saga";
import { watchAddSpecificationAsync } from "./pages/SpecEditor/saga";
import {
  watchSendTraceContinueAsync,
  watchSendTraceStartAsync
} from "./pages/Trace/saga";
import { watchLoginUserAsync } from "./pages/MultiLogin/saga";
import { watchCreateItemAsync, watchFetchSpecAsync } from "./pages/Create/saga";
import {
  watchBuyRequest,
  watchFetchItemInfo,
  watchFetchOwnedItems,
  watchFetchPendingItems,
  watchConfirmRequest,
  watchFetchAcceptedItems,
  watchFetchSpecAsync as watchFetchSpecAsync2,
  watchSellRequest
} from "./pages/Sales/saga";
import {
  watchFetchHandlingProcesses,
  watchFetchHandlingProcessesInfo,
  watchFetchOwningItemInfo,
  watchFetchOwningItems
} from "./pages/Watchlist/saga";

export default function* rootSaga() {
  yield all([
    watchSendInvokeAsync(),
    watchSendQueryAsync(),
    watchCreateUserAsync(),
    watchAddSpecificationAsync(),
    watchSendTraceStartAsync(),
    watchSendTraceContinueAsync(),
    watchLoginUserAsync(),
    watchCreateItemAsync(),
    watchFetchSpecAsync(),
    watchFetchItemInfo(),
    watchFetchOwnedItems(),
    watchFetchPendingItems(),
    watchFetchAcceptedItems(),
    watchFetchSpecAsync2(),
    watchSellRequest(),
    watchBuyRequest(),
    watchConfirmRequest(),
    watchFetchHandlingProcesses(),
    watchFetchHandlingProcessesInfo(),
    watchFetchOwningItemInfo(),
    watchFetchOwningItems()
  ]);
}
