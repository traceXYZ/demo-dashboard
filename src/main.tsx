import throttle from "lodash.throttle";
import * as React from "react";
import { render } from "react-dom";

import { history } from "./services";
import routes from "./routes";
import { configureStore } from "./store";
import saga from "./saga";
import { loadState, saveState } from "./services/localStorage";

interface XWindow extends Window {
  __INITIAL_STATE__: any;
}

const initialState = loadState() || (window as XWindow).__INITIAL_STATE__;

const store = configureStore(initialState);
store.runSaga(saga);

store.subscribe(
  throttle(() => {
    saveState({
      logins: { ...store.getState().logins, loading: false, error: "" }
    });
  }, 1000)
);

import Root from "./Root";

render(
  <Root history={history} routes={routes} store={store} />,
  document.getElementById("app")
);
