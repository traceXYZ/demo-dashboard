import * as React from "react";
import logo from "./logo.jpeg";

export default class HomePage extends React.Component {
  render() {
    return (
      <div className="absolute-center">
        <img src={logo} alt="logo" />
      </div>
    );
  }
}
