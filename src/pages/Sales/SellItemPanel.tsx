import * as React from "react";
import { Row } from "../../components/Layout/Flex";
import {
  FormGroup,
  InputGroup,
  ButtonGroup,
  Button,
  Card,
  Classes,
  ProgressBar,
  Callout
} from "@blueprintjs/core";
import SpecSelector from "../../components/SpecSelector";
import OwnedItemSelector from "./OwnedItemsSelector";
import { Item } from "../Create/Item";
import { SellingProcess } from "./SellingProcess";
import { Specification } from "../Create/CreateTypes";
import { User } from "../MultiLogin/MultiLoginTypes";
import { OwnedItemProxy, OwnedItem } from "./actions";

interface ISellItemPanelProps {
  user: User;

  chaincodeId: string;
  onChaincodeIdChange: (id) => void;

  fcn: string;
  onFcnChange: (event) => void;

  specifications: Array<Specification>;
  spec: Specification;
  onSpecSelect: (spec: Specification) => void;
  onFetchSpecifications: (token: string) => void;

  buyer: string;
  onBuyerChange: (event) => void;

  ownedItemProxies: Array<OwnedItemProxy>;
  ownedItemProxy: OwnedItemProxy;
  ownedItems: Array<OwnedItem>;
  onOwnedItemSelect: (oi: OwnedItemProxy) => void;
  onFetchOwnedItems: (token: string) => void;

  item: string;
  onItemChange: (json: string) => void;

  sellingProcess: string;
  onSellingProcessChange: (json: string) => void;

  onSell: () => void;

  loading: boolean;
  result: string;
  error: string;
}

class SellItemPanel extends React.Component<ISellItemPanelProps> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <Row>
          <FormGroup
            style={{ flexGrow: 1 }}
            helperText="Enter the name of the chaincode assigned to you"
            label="Chaincode Id"
            labelFor="chaincode-id-input"
            labelInfo="(required)"
          >
            <InputGroup
              id="chaincode-id-input"
              placeholder="milkproducer_cc"
              value={this.props.chaincodeId}
              onChange={this.props.onChaincodeIdChange}
            />
          </FormGroup>

          <FormGroup
            style={{ flexGrow: 1 }}
            helperText="Enter the name of the function assigned to you"
            label="Function"
            labelFor="chaincode-function-input"
            labelInfo="(required)"
          >
            <InputGroup
              id="chaincode-function-input"
              placeholder="sell_milk"
              value={this.props.fcn}
              onChange={this.props.onFcnChange}
            />
          </FormGroup>

          <FormGroup
            style={{ flexGrow: 1 }}
            helperText="Enter name of the specification you adhere to"
            label="Specification"
            labelFor="chaincode-spec-input"
            labelInfo="(optional)"
          >
            <ButtonGroup id="spec">
              <SpecSelector
                onSpecSelect={this.props.onSpecSelect}
                selectedSpec={this.props.spec}
                specs={this.props.specifications}
              />
              <Button
                icon="refresh"
                intent="warning"
                onClick={() =>
                  this.props.onFetchSpecifications(this.props.user.token)
                }
              />
            </ButtonGroup>
          </FormGroup>

          <FormGroup
            style={{ flexGrow: 1 }}
            helperText="Enter name of the buyer"
            label="Buyer"
            labelFor="chaincode-buyer-input"
            labelInfo="(required)"
          >
            <InputGroup
              id="chaincode-buyer-input"
              placeholder="cheese_producer"
              value={this.props.buyer}
              onChange={this.props.onBuyerChange}
            />
          </FormGroup>
        </Row>

        <ButtonGroup id="owned">
          <OwnedItemSelector
            onOwnedItemSelect={this.props.onOwnedItemSelect}
            selectedOwnedItem={this.props.ownedItemProxy}
            ownedItems={this.props.ownedItemProxies}
          />
          <Button
            icon="refresh"
            intent="warning"
            onClick={() => this.props.onFetchOwnedItems(this.props.user.token)}
          />
        </ButtonGroup>

        {this.props.ownedItemProxy && (
          <Card
            className={
              this.props.ownedItems.filter(
                item => item.id === this.props.ownedItemProxy.id
              ).length < 1
                ? Classes.SKELETON
                : ""
            }
          >
            <h3>{this.props.ownedItemProxy.type}</h3>
            <p>ID: {this.props.ownedItemProxy.id}</p>
            <pre>
              {
                (
                  this.props.ownedItems.filter(
                    item => item.id === this.props.ownedItemProxy.id
                  )[0] || { payload: "loading..." }
                ).payload
              }
            </pre>
          </Card>
        )}

        <Row>
          <Item
            // key={`item-${this.state.exampleName}`}
            json={this.props.item}
            onJsonChange={this.props.onItemChange}
          />
          <SellingProcess
            // key={`pp-${this.state.exampleName}`}
            json={this.props.sellingProcess}
            onJsonChange={this.props.onSellingProcessChange}
          />
        </Row>

        <ProgressBar
          intent="primary"
          animate={this.props.loading}
          stripes={this.props.loading}
        />

        {this.props.error && (
          <Callout intent="danger">{this.props.error}</Callout>
        )}

        {this.props.result && (
          <Callout intent="success">{this.props.result}</Callout>
        )}

        <Button
          onClick={this.props.onSell}
          intent="primary"
          disabled={
            !(
              this.props.buyer &&
              this.props.chaincodeId &&
              this.props.fcn &&
              this.props.item &&
              this.props.sellingProcess
            )
          }
        >
          Sell
        </Button>
      </>
    );
  }
}

export default SellItemPanel;
