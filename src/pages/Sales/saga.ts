import {
  PutEffect,
  ForkEffect,
  put,
  call,
  takeEvery
} from "redux-saga/effects";

import {
  FetchSpecRequestAction,
  receiveFetchSpecifications,
  FetchSpecRequestActionType,
  FetchSpecResponseAction,
  SalesActionType,
  receiveBuyResponse,
  receiveSellResponse,
  receiveFetchPendingItems,
  receiveFetchItemInfo,
  receiveFetchOwnedItems,
  receiveConfirmResponse,
  receiveFetchAcceptedItems
} from "./actions";
import {
  fetchSpecifications,
  buy,
  sell,
  getPendingBuyItems,
  query,
  getOwnedItems,
  confirm,
  getPendingConfirmItems
} from "../../services/api";
import { Specification } from "../Create/CreateTypes";

export function* watchFetchSpecAsync(): Iterator<ForkEffect> {
  yield takeEvery(FetchSpecRequestActionType.FETCH, fetchSpecAsync);
}

function* fetchSpecAsync(
  action: FetchSpecRequestAction
): AsyncIterableIterator<PutEffect<FetchSpecResponseAction>> {
  let json = yield call(fetchSpecifications, action.token);

  try {
    const specs = JSON.parse(json).specifications.map(s => ({
      id: s.spec_id,
      name: s.spec_name
    })) as Array<Specification>;
    yield put(receiveFetchSpecifications(specs));
  } catch (error) {
    yield put(receiveFetchSpecifications(null));
  }
}

export function* watchFetchOwnedItems(): Iterator<ForkEffect> {
  yield takeEvery(SalesActionType.GET_OWNED, fetchOwnedItems);
}

function* fetchOwnedItems(action) {
  try {
    const json = yield call(getOwnedItems, action.token);
    const obj = JSON.parse(json);
    if (obj.error) {
      yield put(receiveFetchOwnedItems(null, obj.error));
    } else {
      const arrOwnedItems = Object.keys(obj).map(k => ({ id: k, ...obj[k] }));
      yield put(receiveFetchOwnedItems(arrOwnedItems, null));
    }
  } catch (error) {
    yield put(receiveFetchOwnedItems(null, error.message));
  }
}

export function* watchFetchItemInfo(): Iterator<ForkEffect> {
  yield takeEvery(SalesActionType.GET_OWNED_INFO, fetchItemInfo);
}

function* fetchItemInfo(action) {
  try {
    const payload = {
      chaincodeId: "storage_cc",
      fcn: "retrieve_direct",
      args: [action.id],
      chainId: "cheeseproduction"
    };
    const json = yield call(query, JSON.stringify(payload), action.token);
    const obj = JSON.parse(json);
    if (obj.error) {
      yield put(receiveFetchItemInfo(null, obj.error));
    } else {
      yield put(
        receiveFetchItemInfo(
          { id: obj.storage_item.id, payload: atob(obj.storage_item.payload) },
          null
        )
      );
    }
  } catch (error) {
    yield put(receiveFetchItemInfo(null, error.message));
  }
}

export function* watchFetchPendingItems(): Iterator<ForkEffect> {
  yield takeEvery(SalesActionType.GET_PENDING, fetchPendingItems);
}

function* fetchPendingItems(action) {
  try {
    const json = yield call(getPendingBuyItems, action.token);
    const obj = JSON.parse(json);
    if (obj.error) {
      yield put(receiveFetchPendingItems(null, obj.error));
    } else {
      yield put(receiveFetchPendingItems(obj.results, null));
    }
  } catch (error) {
    yield put(receiveFetchPendingItems(null, error.message));
  }
}

export function* watchFetchAcceptedItems(): Iterator<ForkEffect> {
  yield takeEvery(SalesActionType.GET_ACCEPTED, fetchAcceptedItems);
}

function* fetchAcceptedItems(action) {
  try {
    const json = yield call(getPendingConfirmItems, action.token);
    const obj = JSON.parse(json);
    if (obj.error) {
      yield put(receiveFetchAcceptedItems(null, obj.error));
    } else {
      yield put(receiveFetchAcceptedItems(obj.results, null));
    }
  } catch (error) {
    yield put(receiveFetchAcceptedItems(null, error.message));
  }
}

export function* watchSellRequest(): Iterator<ForkEffect> {
  yield takeEvery(SalesActionType.SELL_REQ, sellRequest);
}

function* sellRequest(action) {
  try {
    const payload = JSON.stringify(action.payload);
    const json = yield call(sell, payload, action.token);
    const obj = JSON.parse(json);
    if (obj.error) {
      yield put(receiveSellResponse(null, obj.error));
    } else {
      yield put(
        receiveSellResponse("Success. Waiting for the buyer to accept.", null)
      );
    }
  } catch (error) {
    yield put(receiveSellResponse(null, error.message));
  }
}

export function* watchBuyRequest(): Iterator<ForkEffect> {
  yield takeEvery(SalesActionType.BUY_REQ, buyRequest);
}

function* buyRequest(action) {
  try {
    const json = yield call(
      buy,
      JSON.stringify({ id: action.id }),
      action.token
    );
    const obj = JSON.parse(json);
    if (obj.error) {
      yield put(receiveBuyResponse(null, obj.error));
    } else {
      yield put(
        receiveBuyResponse("Success. Waiting for the seller to confirm.", null)
      );
    }
  } catch (error) {
    yield put(receiveBuyResponse(error.message, null));
  }
}

export function* watchConfirmRequest(): Iterator<ForkEffect> {
  yield takeEvery(SalesActionType.CONFIRM_REQ, confirmRequest);
}

function* confirmRequest(action) {
  try {
    const json = yield call(
      confirm,
      JSON.stringify({ id: action.id }),
      action.token
    );
    const obj = JSON.parse(json);
    if (obj.error) {
      yield put(receiveConfirmResponse(null, obj.message));
    } else {
      const storageNetworkResponse = JSON.parse(obj.storage_network_response);
      yield put(receiveConfirmResponse(storageNetworkResponse.message, null));
    }
  } catch (error) {
    yield put(receiveConfirmResponse(error.message, null));
  }
}
