import * as React from "react";
import JsonEditor from "../../components/JsonEditor";
import { H5 } from "@blueprintjs/core";
import { Column } from "../../components/Layout/Flex";

interface ISellingProcessProps {
  json: string;
  onJsonChange: (json: string) => void;
}

interface ISellingProcessState {}

export class SellingProcess extends React.Component<
  ISellingProcessProps,
  ISellingProcessState
> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Column>
        <H5>Selling Process Details</H5>
        <JsonEditor
          height="200px"
          json={this.props.json}
          onChange={this.props.onJsonChange}
        />
      </Column>
    );
  }
}
