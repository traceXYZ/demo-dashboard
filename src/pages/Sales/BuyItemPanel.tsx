import * as React from "react";
import { Select, ItemRenderer } from "@blueprintjs/select";
import {
  MenuItem,
  Button,
  ButtonGroup,
  Callout,
  ProgressBar
} from "@blueprintjs/core";
import { User } from "../MultiLogin/MultiLoginTypes";

type Pending = {
  _id: string;
  item: Object;
  sellingProcess: Object;
};

const PendingItemSelect = Select.ofType<Pending>();

const renderPendingItem: ItemRenderer<Pending> = (
  pendingItem,
  { handleClick, modifiers }
) => {
  return (
    <MenuItem
      active={modifiers.active}
      key={pendingItem._id}
      onClick={handleClick}
      text={pendingItem.item.itemType}
    />
  );
};

interface IPendingItemSelectorProps {
  onPendingItemSelect: (spec: Pending) => void;

  pendingItems: Array<Pending>;
  selectedPendingItem: Pending;
}

class PendingItemSelector extends React.Component<IPendingItemSelectorProps> {
  constructor(props) {
    super(props);
  }

  onPendingItemSelect = item => {
    this.props.onPendingItemSelect(item);
  };

  render() {
    return (
      <div>
        <PendingItemSelect
          onItemSelect={this.onPendingItemSelect}
          itemRenderer={renderPendingItem}
          itemPredicate={(q, x, i) => x.item.itemType.includes(q)}
          items={this.props.pendingItems}
        >
          <Button
            rightIcon="caret-down"
            icon="box"
            intent="none"
            text={
              (this.props.selectedPendingItem &&
                this.props.selectedPendingItem.item &&
                JSON.stringify(this.props.selectedPendingItem.item)) ||
              "Select item to buy"
            }
          />
        </PendingItemSelect>
      </div>
    );
  }
}

interface IBuyItemPanelProps {
  error: string;
  result: string;
  loading: boolean;

  user: User;

  pendingItems: Array<Pending>;
  pendingItem: Pending;

  onPendingItemSelect: (p: Pending) => void;
  onFetchPendingItems: (token: string) => void;
  onBuy: () => void;
}

class BuyItemPanel extends React.Component<IBuyItemPanelProps> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <ButtonGroup id="owned">
          <PendingItemSelector
            onPendingItemSelect={this.props.onPendingItemSelect}
            selectedPendingItem={this.props.pendingItem}
            pendingItems={this.props.pendingItems}
          />
          <Button
            icon="refresh"
            intent="warning"
            onClick={() =>
              this.props.onFetchPendingItems(this.props.user.token)
            }
          />
        </ButtonGroup>

        <ProgressBar
          intent="primary"
          animate={this.props.loading}
          stripes={this.props.loading}
        />

        {this.props.error && (
          <Callout intent="danger">{this.props.error}</Callout>
        )}

        {this.props.result && (
          <Callout intent="success">{this.props.result}</Callout>
        )}

        <Button
          onClick={this.props.onBuy}
          intent="primary"
          disabled={!this.props.pendingItem}
        >
          Buy
        </Button>
      </>
    );
  }
}

export default BuyItemPanel;
