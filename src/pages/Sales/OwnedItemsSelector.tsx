import * as React from "react";
import { Select, ItemRenderer } from "@blueprintjs/select";
import { Button, MenuItem } from "@blueprintjs/core";

import { OwnedItemProxy } from "./actions";

const OwnedItemSelect = Select.ofType<OwnedItemProxy>();

const renderOwnedItem: ItemRenderer<OwnedItemProxy> = (
  ownedItem,
  { handleClick, modifiers }
) => {
  return (
    <MenuItem
      active={modifiers.active}
      key={ownedItem.id}
      onClick={handleClick}
      text={ownedItem.type}
    />
  );
};

interface IOwnedItemSelectorProps {
  onOwnedItemSelect: (spec: OwnedItemProxy) => void;

  ownedItems: Array<OwnedItemProxy>;
  selectedOwnedItem: OwnedItemProxy;
}

class OwnedItemSelector extends React.Component<IOwnedItemSelectorProps> {
  constructor(props) {
    super(props);
  }

  onOwnedItemSelect = spec => {
    this.props.onOwnedItemSelect(spec);
  };

  render() {
    return (
      <div>
        <OwnedItemSelect
          onItemSelect={this.onOwnedItemSelect}
          itemRenderer={renderOwnedItem}
          items={this.props.ownedItems}
        >
          <Button
            rightIcon="caret-down"
            icon="box"
            intent="none"
            text={
              (this.props.selectedOwnedItem &&
                this.props.selectedOwnedItem.type) ||
              "Select item to sell"
            }
          />
        </OwnedItemSelect>
      </div>
    );
  }
}

export default OwnedItemSelector;
