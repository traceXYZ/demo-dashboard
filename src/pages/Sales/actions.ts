import { Specification } from "../Create/CreateTypes";

export enum FetchSpecRequestActionType {
  FETCH = "SPEC_FETCH_REQUEST_SEND"
}

export enum FetchSpecResponseActionType {
  FETCH = "SPEC_FETCH_RESPONSE_RECEIVE"
}

export type FetchSpecRequestAction = {
  type: FetchSpecRequestActionType;
  token: string;
};

export type FetchSpecResponseAction = {
  type: FetchSpecResponseActionType;
  payload: Array<Specification>;
};

export type FetchSpecAction = FetchSpecRequestAction | FetchSpecResponseAction;

export function sendFetchSpecifications(token: string): FetchSpecRequestAction {
  return { type: FetchSpecRequestActionType.FETCH, token };
}

export function receiveFetchSpecifications(
  result: Array<Specification>
): FetchSpecResponseAction {
  return {
    type: FetchSpecResponseActionType.FETCH,
    payload: result
  };
}

export enum SalesActionType {
  GET_OWNED = "OWNED_FETCH_REQUEST_SEND",
  GOT_OWNED = "OWNED_FETCH_RESPONSE_RECEIVE",

  GET_OWNED_INFO = "OWNED_INFO_REQUEST_SEND",
  GOT_OWNED_INFO = "OWNED_INFO_RESPONSE_RECEIVE",

  GET_PENDING = "PENDING_FETCH_REQUEST_SEND",
  GOT_PENDING = "PENDING_FETCH_RESPONSE_RECEIVE",

  GET_ACCEPTED = "ACCEPTED_FETCH_REQUEST_SEND",
  GOT_ACCEPTED = "ACCEPTED_FETCH_RESPONSE_RECEIVE",

  SELL_REQ = "SELL_ITEM_REQUEST_SEND",
  SELL_RES = "SELL_ITEM_RESPONSE_RECEIVE",

  BUY_REQ = "BUY_ITEM_REQUEST_SEND",
  BUY_RES = "BUY_ITEM_RESPONSE_RECEIVE",

  CONFIRM_REQ = "CONFIRM_ITEM_REQUEST_SEND",
  CONFIRM_RES = "CONFIRM_ITEM_RESPONSE_RECEIVE"
}

export function sendFetchOwnedItems(token: string) {
  return { type: SalesActionType.GET_OWNED, token };
}

export type OwnedItemProxy = { id: string; type: string };

export function receiveFetchOwnedItems(
  result: OwnedItemProxy[],
  error: string
) {
  return { type: SalesActionType.GOT_OWNED, result, error };
}

export function sendFetchItemInfo(id: string, token: string) {
  return { type: SalesActionType.GET_OWNED_INFO, id, token };
}

export type OwnedItem = { id: string; payload: string };

export function receiveFetchItemInfo(result: OwnedItem, error: string) {
  return { type: SalesActionType.GOT_OWNED_INFO, result, error };
}

export function sendFetchPendingItems(token: string) {
  return { type: SalesActionType.GET_PENDING, token };
}

export type PendingItem = {
  _id: string;
  item: Object;
  sellingProcess: Object;
};

export function receiveFetchPendingItems(result: PendingItem[], error: string) {
  return { type: SalesActionType.GOT_PENDING, result, error };
}

export function sendFetchAcceptedItems(token: string) {
  return { type: SalesActionType.GET_ACCEPTED, token };
}

export type AcceptedItem = {
  _id: string;
  item: Object;
  sellingProcess: Object;
};

export function receiveFetchAcceptedItems(
  result: AcceptedItem[],
  error: string
) {
  return { type: SalesActionType.GOT_ACCEPTED, result, error };
}

export function sendSellRequest(
  itemId: string,
  item: string,
  sellingProcess: string,
  specification: string,
  chaincodeId: string,
  fcn: string,
  buyer: string,
  token: string
) {
  return {
    type: SalesActionType.SELL_REQ,
    payload: {
      itemId,
      item,
      sellingProcess,
      specification,
      chaincodeId,
      fcn,
      buyer
    },
    token
  };
}

export function receiveSellResponse(result: string, error: string) {
  return { type: SalesActionType.SELL_RES, result, error };
}

export function sendBuyRequest(id: string, token: string) {
  return { type: SalesActionType.BUY_REQ, id, token };
}

export function receiveBuyResponse(result: string, error: string) {
  return { type: SalesActionType.BUY_RES, result, error };
}

export function sendConfirmRequest(id: string, token: string) {
  return { type: SalesActionType.CONFIRM_REQ, id, token };
}

export function receiveConfirmResponse(result: string, error: string) {
  return { type: SalesActionType.CONFIRM_RES, result, error };
}
