import { connect } from "react-redux";
import { User } from "../MultiLogin/MultiLoginTypes";

import Sales from "./Sales";
import {
  sendFetchSpecifications,
  sendFetchOwnedItems,
  sendSellRequest,
  sendFetchPendingItems,
  sendBuyRequest,
  sendFetchItemInfo,
  sendFetchAcceptedItems,
  sendConfirmRequest
} from "./actions";

const mapStateToProps = (state: {
  logins: { users: Array<User> };
  sales: any;
}) => {
  return {
    users: state.logins.users,
    ...state.sales
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchSpecifications: (token: string) => {
      dispatch(sendFetchSpecifications(token));
    },

    onFetchOwnedItems: (token: string) => {
      dispatch(sendFetchOwnedItems(token));
    },

    onFetchItemInfo: (id: string, token: string) => {
      dispatch(sendFetchItemInfo(id, token));
    },

    onSellClick: (
      itemId: string,
      item: string,
      sellingProcess: string,
      specification: string,
      chaincodeId: string,
      fcn: string,
      buyer: string,
      token: string
    ) => {
      dispatch(
        sendSellRequest(
          itemId,
          item,
          sellingProcess,
          specification,
          chaincodeId,
          fcn,
          buyer,
          token
        )
      );
    },

    onFetchPendingItems: (token: string) => {
      dispatch(sendFetchPendingItems(token));
    },

    onAcceptClick: (id: string, token: string) => {
      dispatch(sendBuyRequest(id, token));
    },

    onFetchAcceptedItems: (token: string) => {
      dispatch(sendFetchAcceptedItems(token));
    },

    onConfirmClick: (id: string, token: string) => {
      dispatch(sendConfirmRequest(id, token));
    }
  };
};

const SalesPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(Sales);

export default SalesPage;
