import * as React from "react";
import { Select, ItemRenderer } from "@blueprintjs/select";
import {
  MenuItem,
  Button,
  ButtonGroup,
  Callout,
  ProgressBar
} from "@blueprintjs/core";
import { User } from "../MultiLogin/MultiLoginTypes";

type Accepted = {
  _id: string;
  item: Object;
  sellingProcess: Object;
};

const AcceptedItemSelect = Select.ofType<Accepted>();

const renderAcceptedItem: ItemRenderer<Accepted> = (
  acceptedItem,
  { handleClick, modifiers }
) => {
  return (
    <MenuItem
      active={modifiers.active}
      key={acceptedItem._id}
      onClick={handleClick}
      text={acceptedItem.item.itemType}
    />
  );
};

interface IAcceptedItemSelectorProps {
  onAcceptedItemSelect: (spec: Accepted) => void;

  acceptedItems: Array<Accepted>;
  selectedAcceptedItem: Accepted;
}

class AcceptedItemSelector extends React.Component<IAcceptedItemSelectorProps> {
  constructor(props) {
    super(props);

    console.log(props);
  }

  onAcceptedItemSelect = item => {
    this.props.onAcceptedItemSelect(item);
  };

  render() {
    return (
      <div>
        <AcceptedItemSelect
          onItemSelect={this.onAcceptedItemSelect}
          itemRenderer={renderAcceptedItem}
          itemPredicate={(q, x, i) => x.item.itemType.includes(q)}
          items={this.props.acceptedItems}
        >
          <Button
            rightIcon="caret-down"
            icon="box"
            intent="none"
            text={
              (this.props.selectedAcceptedItem &&
                this.props.selectedAcceptedItem.item &&
                JSON.stringify(this.props.selectedAcceptedItem.item)) ||
              "Select item to confirm"
            }
          />
        </AcceptedItemSelect>
      </div>
    );
  }
}

interface IConfirmItemPanelProps {
  error: string;
  result: string;
  loading: boolean;

  user: User;

  acceptedItems: Array<Accepted>;
  acceptedItem: Accepted;

  onAcceptedItemSelect: (p: Accepted) => void;
  onFetchAcceptedItems: (token: string) => void;
  onConfirm: () => void;
}

class ConfirmItemPanel extends React.Component<IConfirmItemPanelProps> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <ButtonGroup id="owned">
          <AcceptedItemSelector
            onAcceptedItemSelect={this.props.onAcceptedItemSelect}
            selectedAcceptedItem={this.props.acceptedItem}
            acceptedItems={this.props.acceptedItems}
          />
          <Button
            icon="refresh"
            intent="warning"
            onClick={() =>
              this.props.onFetchAcceptedItems(this.props.user.token)
            }
          />
        </ButtonGroup>

        <ProgressBar
          intent="primary"
          animate={this.props.loading}
          stripes={this.props.loading}
        />

        {this.props.error && (
          <Callout intent="danger">{this.props.error}</Callout>
        )}

        {this.props.result && (
          <Callout intent="success">{this.props.result}</Callout>
        )}

        <Button
          onClick={this.props.onConfirm}
          intent="primary"
          disabled={!this.props.acceptedItem}
        >
          Confirm
        </Button>
      </>
    );
  }
}

export default ConfirmItemPanel;
