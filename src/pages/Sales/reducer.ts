import {
  SalesActionType,
  FetchSpecRequestActionType,
  FetchSpecResponseActionType
} from "./actions";

const defaultState = {
  specifications: [],

  ownedItemProxies: [],
  ownedItems: [],
  pendingItems: [],
  acceptedItems: [],

  loadingSell: false,
  errorSell: "",
  resultSell: "",

  loadingBuy: false,
  errorBuy: "",
  resultBuy: "",

  loadingConfirm: false,
  errorConfirm: "",
  resultConfirm: ""
};

export function sales(state = defaultState, action) {
  switch (action.type) {
    case FetchSpecRequestActionType.FETCH: // Saga
      return state;

    case SalesActionType.GET_OWNED: // Saga
      return state;
    case SalesActionType.GET_OWNED_INFO: // Saga
      return state;
    case SalesActionType.GET_PENDING: // Saga
      return state;
    case SalesActionType.GET_ACCEPTED:
      return state;

    case SalesActionType.SELL_REQ: // Saga
      return { ...state, loadingSell: true, errorSell: "", resultSell: "" };
    case SalesActionType.BUY_REQ: // Saga
      return { ...state, loadingBuy: true, errorBuy: "", resultBuy: "" };
    case SalesActionType.CONFIRM_REQ: // Saga
      return {
        ...state,
        loadingConfirm: true,
        errorConfirm: "",
        resultConfirm: ""
      };

    case FetchSpecResponseActionType.FETCH:
      return { ...state, specifications: action.payload };

    case SalesActionType.GOT_OWNED:
      if (action.error) {
        return state;
      }
      return {
        ...state,
        ownedItemProxies: action.result || state.ownedItemProxies
      };

    case SalesActionType.GOT_OWNED_INFO:
      if (action.error) {
        return state;
      }
      return {
        ...state,
        ownedItems: [
          ...state.ownedItems.filter(id => id !== action.result.id),
          action.result
        ]
      };

    case SalesActionType.GOT_PENDING:
      if (action.error) {
        return state;
      }
      return {
        ...state,
        pendingItems: action.result.map(result => ({
          item: JSON.parse(result.item),
          sellingProcess: JSON.parse(result.sellingProcess)
        }))
      };

    case SalesActionType.GOT_ACCEPTED:
      if (action.error) {
        return state;
      }
      return {
        ...state,
        acceptedItems: action.result.map(result => ({
          item: JSON.parse(result.item),
          sellingProcess: JSON.parse(result.sellingProcess)
        }))
      };

    case SalesActionType.SELL_RES:
      if (action.error) {
        return { ...state, loadingSell: false, errorSell: action.error };
      }
      return { ...state, resultSell: action.result, loadingSell: false };

    case SalesActionType.BUY_RES:
      if (action.error) {
        return { ...state, loadingBuy: false, errorBuy: action.error };
      }
      return { ...state, resultBuy: action.result, loadingBuy: false };

    case SalesActionType.CONFIRM_RES:
      if (action.error) {
        return { ...state, loadingConfirm: false, errorConfirm: action.error };
      }
      return { ...state, resultConfirm: action.result, loadingConfirm: false };

    default:
      return state;
  }
}
