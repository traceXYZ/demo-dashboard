import * as React from "react";
import { Tabs, Tab } from "@blueprintjs/core";

import { User } from "../MultiLogin/MultiLoginTypes";
import { Specification } from "../Create/CreateTypes";
import UserSelector from "../../components/UserSelector";

import { OwnedItemProxy } from "./actions";
import SellItemPanel from "./SellItemPanel";
import BuyItemPanel from "./BuyItemPanel";
import ConfirmItemPanel from "./ConfirmItemPanel";

interface ISalesPageProps {
  onFetchSpecifications: (token: string) => void;

  onFetchOwnedItems: (token: string) => void;

  onFetchItemInfo: (id: string, token: string) => void;
  // Fetch additional information for owned items

  onSellClick: (
    itemId: string,
    item: string,
    sellingProcess: string,
    specification: string,
    chaincodeId: string,
    fcn: string,
    buyer: string,
    token: string
  ) => void;
  // Sell something

  onFetchPendingItems: (token: string) => void;

  onAcceptClick: (id: string, token: string) => void;
  // Buy something

  onFetchAcceptedItems: (token: string) => void;

  onConfirmClick: (id: string, token: string) => void;
  // Confirm by seller

  users: Array<User>;
  specifications: Array<Specification>;

  ownedItemProxies: Array<{ id: string; type: string }>;
  ownedItems: Array<{ id: string; payload: string }>;

  pendingItems: Array<{
    _id: string;
    item: Object;
    sellingProcess: Object;
  }>;

  acceptedItems: Array<{
    _id: string;
    item: Object;
    sellingProcess: Object;
  }>;

  loadingSell: boolean;
  errorSell?: string;
  resultSell?: string;

  loadingBuy: boolean;
  errorBuy?: string;
  resultBuy?: string;

  loadingConfirm: boolean;
  errorConfirm?: string;
  resultConfirm?: string;
}

interface ISalesPageState {
  user: User;
  spec: Specification;

  item: string;
  sellingProcess: string;
  fcn: string;
  chaincodeId: string;
  buyer: string;
  selectedTabId: string;

  ownedItemProxy: OwnedItemProxy;
  pendingItem: any;
  acceptedItem: any;
}

class SalesPage extends React.Component<ISalesPageProps, ISalesPageState> {
  constructor(props) {
    super(props);

    this.state = {
      user: this.props.users && this.props.users[0],
      spec: this.props.specifications && this.props.specifications[0],
      item: `{
  "itemType": "milk",
  "qty": 5,
  "item_image": "IMG"
}`,
      sellingProcess: `{
  "processType": "Milk Selling",
  "sold_units": "L",
  "farmid": "FARM001",
  "location": "LOCAT1",
  "pos_machine_id": "MACH001"
}`,
      chaincodeId: "milkproducer_cc",
      fcn: "sell_milk",
      buyer: "cheese_producer",
      selectedTabId: "sell-tab",

      ownedItemProxy: null,
      pendingItem: null,
      acceptedItem: null
    };
  }

  componentDidMount() {
    if (!this.props.specifications || this.props.specifications.length < 1) {
      if (this.state.user) {
        this.props.onFetchSpecifications(this.state.user.token);
      }
    }
  }

  onUserSelect = user => {
    this.setState({ user });
    this.props.onFetchOwnedItems(user.token);
    this.props.onFetchPendingItems(user.token);
    this.props.onFetchAcceptedItems(user.token);
    this.setState({ ownedItemProxy: null });
  };

  onSpecSelect = spec => {
    this.setState({ spec });
  };

  onOwnedItemSelect = ownedItemProxy => {
    this.setState({ ownedItemProxy });
    if (
      this.props.ownedItems.filter(item => item.id === ownedItemProxy.id)
        .length < 1
    ) {
      this.props.onFetchItemInfo(ownedItemProxy.id, this.state.user.token);
    }
  };

  onPendingItemSelect = pendingItem => {
    this.setState({ pendingItem });
  };

  onAcceptedItemSelect = acceptedItem => {
    this.setState({ acceptedItem });
  };

  onChaincodeIdChange = event => {
    this.setState({ chaincodeId: event.target.value });
  };

  onFcnChange = event => {
    this.setState({ fcn: event.target.value });
  };

  onItemChange = (json: string) => {
    this.setState({ item: json });
  };

  onSellingProcessChange = (json: string) => {
    this.setState({ sellingProcess: json });
  };

  onBuyerChange = event => {
    this.setState({ buyer: event.target.value });
  };

  onTabChange = selectedTabId => {
    this.setState({ selectedTabId });
  };

  onSellClick = () => {
    const {
      item,
      ownedItemProxy,
      spec,
      user,
      sellingProcess,
      buyer,
      fcn,
      chaincodeId
    } = this.state;
    this.props.onSellClick(
      (ownedItemProxy && ownedItemProxy.id) || "",
      item,
      sellingProcess,
      (spec && spec.id) || "",
      chaincodeId,
      fcn,
      buyer,
      user.token
    );
  };

  onBuyClick = () => {
    if (this.state.pendingItem) {
      this.props.onAcceptClick(
        this.state.pendingItem._id,
        this.state.user.token
      );
    }
  };

  onConfirmClick = () => {
    if (this.state.acceptedItem) {
      this.props.onConfirmClick(
        this.state.acceptedItem._id,
        this.state.user.token
      );
    }
  };

  render() {
    return (
      <div>
        <UserSelector
          users={this.props.users}
          onUserSelect={this.onUserSelect}
          selectedUser={this.state.user}
        />
        <Tabs
          id="sales-tabs"
          onChange={this.onTabChange}
          selectedTabId={this.state.selectedTabId}
        >
          <Tab
            id="sell-tab"
            title="Sell"
            panel={
              <SellItemPanel
                buyer={this.state.buyer}
                chaincodeId={this.state.chaincodeId}
                fcn={this.state.fcn}
                item={this.state.item}
                onBuyerChange={this.onBuyerChange}
                onChaincodeIdChange={this.onChaincodeIdChange}
                onFcnChange={this.onFcnChange}
                onFetchOwnedItems={this.props.onFetchOwnedItems}
                onFetchSpecifications={this.props.onFetchSpecifications}
                onItemChange={this.onItemChange}
                onOwnedItemSelect={this.onOwnedItemSelect}
                onSellingProcessChange={this.onSellingProcessChange}
                onSpecSelect={this.onSpecSelect}
                ownedItemProxies={this.props.ownedItemProxies}
                ownedItemProxy={this.state.ownedItemProxy}
                ownedItems={this.props.ownedItems}
                sellingProcess={this.state.sellingProcess}
                spec={this.state.spec}
                specifications={this.props.specifications}
                user={this.state.user}
                onSell={this.onSellClick}
                error={this.props.errorSell}
                loading={this.props.loadingSell}
                result={this.props.resultSell}
              />
            }
          />
          <Tab
            id="buy-tab"
            title="Buy"
            panel={
              <BuyItemPanel
                user={this.state.user}
                error={this.props.errorBuy}
                loading={this.props.loadingBuy}
                result={this.props.resultBuy}
                pendingItems={this.props.pendingItems}
                pendingItem={this.state.pendingItem}
                onFetchPendingItems={this.props.onFetchPendingItems}
                onPendingItemSelect={this.onPendingItemSelect}
                onBuy={this.onBuyClick}
              />
            }
          />
          <Tab
            id="confirm-tab"
            title="Confirm"
            panel={
              <ConfirmItemPanel
                user={this.state.user}
                error={this.props.errorConfirm}
                loading={this.props.loadingConfirm}
                result={this.props.resultConfirm}
                acceptedItems={this.props.acceptedItems}
                acceptedItem={this.state.acceptedItem}
                onFetchAcceptedItems={this.props.onFetchAcceptedItems}
                onAcceptedItemSelect={this.onAcceptedItemSelect}
                onConfirm={this.onConfirmClick}
              />
            }
          />
        </Tabs>
      </div>
    );
  }
}

export default SalesPage;
