import * as React from "react";

import QuotaEntryViewer from "./QuotaEntryViewer";
import QuotaEntryEditor from "./QuotaEntryEditor";
import { IQuotaEntry } from "./SpecEditorTypes";

interface IQuotaEntryPanelProps {
  entries: Array<IQuotaEntry>;
  onEntriesChange(QuotaEntries: Array<IQuotaEntry>): void;
}

export default class QuotaEntryPanel extends React.Component<
  IQuotaEntryPanelProps
> {
  constructor(props) {
    super(props);
  }

  onClickEditEntry = () => {};

  onClickDeleteEntry = (id: number) => {
    this.props.onEntriesChange(
      this.props.entries.filter((entry, i) => i !== id)
    );
  };

  onClickAddEntry = (entry: IQuotaEntry) => {
    this.props.onEntriesChange([...this.props.entries, entry]);
  };

  render() {
    return (
      <>
        {this.props.entries.map((entry, i) => (
          <QuotaEntryViewer
            QuotaEntry={entry}
            editButtonText="Edit"
            onEditButtonClick={this.onClickEditEntry}
            deleteButtonText="Delete"
            onDeleteButtonClick={this.onClickDeleteEntry}
            key={i}
            id={i}
          />
        ))}

        <QuotaEntryEditor
          okayButtonText="Add"
          onOkayButtonClick={this.onClickAddEntry}
          cancelButtonText="Clear"
        />
      </>
    );
  }
}
