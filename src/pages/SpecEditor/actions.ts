export enum AddSpecificationRequestActionType {
  ADD = "SPEC_ADD_REQUEST_SEND"
}

export enum AddSpecificationResponseActionType {
  ADD = "SPEC_ADD_RESPONSE_RECEIVE"
}

export type AddSpecificationRequestAction = {
  type: AddSpecificationRequestActionType;
  payload: { specification: string; name: string; token: string };
};

export type AddSpecificationResponseAction = {
  type: AddSpecificationResponseActionType;
  result?: string;
  error?: string;
};

export type AddSpecificationAction =
  | AddSpecificationRequestAction
  | AddSpecificationResponseAction;

export function sendAddSpecification(
  specification: string,
  name: string,
  token: string
): AddSpecificationRequestAction {
  return {
    type: AddSpecificationRequestActionType.ADD,
    payload: { specification, name, token }
  };
}

export function receiveAddSpecification(
  result: string
): AddSpecificationResponseAction {
  return {
    type: AddSpecificationResponseActionType.ADD,
    result
  };
}
