import * as React from "react";
import {
  InputGroup,
  FormGroup,
  NumericInput,
  Callout,
  Divider,
  Button,
  Card,
  H5,
  ButtonGroup
} from "@blueprintjs/core";

import { IRatioEntry } from "./SpecEditorTypes";

interface IRatioEntryEditorProps {
  ratioEntry?: IRatioEntry;
  okayButtonText: string;
  onOkayButtonClick(IRatioEntry): void;
  cancelButtonText: string;
  onCancelButtonClick?(): void;
}

const initialState = {
  fieldPath1: "",
  fieldPath2: "",
  ratio: 1,
  tolerance: 0.1
};

export default class RatioEntryEditor extends React.Component<
  IRatioEntryEditorProps,
  IRatioEntry
> {
  constructor(props) {
    super(props);

    this.state = props.ratioEntry || initialState;
  }

  onOkayButtonClick = () => {
    this.props.onOkayButtonClick(this.state);
  };

  onCancelButtonClick = () => {
    if (!this.props.onOkayButtonClick) {
      this.setState(initialState);
    } else {
      this.props.onCancelButtonClick();
    }
  };

  onPayloadPathChange = event => {
    this.setState({
      payloadPath: event.target.value
    });
  };

  onFieldPath1Change = event => {
    this.setState({
      fieldPath1: event.target.value
    });
  };

  onFieldPath2Change = event => {
    this.setState({
      fieldPath2: event.target.value
    });
  };

  onRatioChange = valueAsNumber => {
    this.setState({
      ratio: valueAsNumber
    });
  };

  onToleranceChange = valueAsNumber => {
    this.setState({
      tolerance: valueAsNumber
    });
  };

  render() {
    return (
      <Card>
        <H5>Add New Entry</H5>

        <FormGroup
          helperText="JSONPath expression for the first variable"
          label="Numerator"
          labelFor="edit-ratio-field-path-1"
        >
          <InputGroup
            id="edit-ratio-payload-path"
            placeholder={"$.created_item"}
            onChange={this.onPayloadPathChange}
            value={this.state.payloadPath}
          />
        </FormGroup>
        <FormGroup
          helperText="JSONPath expression for the first variable"
          label="Numerator"
          labelFor="edit-ratio-field-path-1"
        >
          <InputGroup
            id="edit-ratio-field-path-1"
            placeholder={"$.qty_1"}
            onChange={this.onFieldPath1Change}
            value={this.state.fieldPath1}
          />
        </FormGroup>

        <FormGroup
          helperText="JSONPath expression for the second variable"
          label="Denominator"
          labelFor="edit-ratio-field-path-2"
        >
          <InputGroup
            id="edit-ratio-field-path-2"
            placeholder={"$.qty_2"}
            onChange={this.onFieldPath2Change}
            value={this.state.fieldPath2}
          />
        </FormGroup>

        <FormGroup
          helperText="Allowed ratio between the two variables"
          label="Ratio"
          labelFor="edit-ratio-ratio"
        >
          <NumericInput
            id="edit-ratio-ratio"
            placeholder="10"
            onValueChange={this.onRatioChange}
            value={this.state.ratio}
            min={0}
          />
        </FormGroup>

        <FormGroup
          helperText="Tolerance on ratio"
          label="Tolerance"
          labelFor="edit-ratio-tolerance"
        >
          <NumericInput
            id="edit-ratio-tolerance"
            placeholder="0.1"
            majorStepSize={0.1}
            stepSize={0.05}
            minorStepSize={0.02}
            min={0}
            value={this.state.tolerance}
            onValueChange={this.onToleranceChange}
          />
        </FormGroup>

        <Divider />

        <Callout intent="primary" title="Preview">
          {`${this.state.fieldPath1} / ${this.state.fieldPath2} = ${
            this.state.ratio
          } ± ${this.state.tolerance * this.state.ratio}`}
        </Callout>

        <ButtonGroup>
          <Button intent="none" onClick={this.onCancelButtonClick}>
            {this.props.cancelButtonText}
          </Button>
          <Button intent="primary" onClick={this.onOkayButtonClick}>
            {this.props.okayButtonText}
          </Button>
        </ButtonGroup>
      </Card>
    );
  }
}
