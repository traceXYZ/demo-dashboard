export enum ValueType {
  NUMBER = "NUMBER",
  TEXT = "TEXT"
}

export enum StringComparisonOperatorType {
  EQUALS = "EQ"
}

export enum NumberComparisonOperatorType {
  EQUAL = "=",
  GREATER_THAN = ">",
  GREATER_THAN_OR_EQUAL = ">=",
  LESS_THAN = "<",
  LESS_THAN_OR_EQUAL = "<="
}

export interface ICompareEntry {
  payloadPath: string;
  fieldPath: string;
  type: ValueType;
  operator: StringComparisonOperatorType | NumberComparisonOperatorType;
  textValue: string;
  numberValue: number;
}

export interface IQuotaEntry {
  payloadPath: string;
  fieldPath: string;
}

export interface IRatioEntry {
  payloadPath: string;
  fieldPath1: string;
  fieldPath2: string;
  ratio: number;
  tolerance: number;
}
