import * as React from "react";
import {
  HTMLSelect,
  InputGroup,
  NumericInput,
  FormGroup,
  ControlGroup,
  ButtonGroup,
  Button,
  Callout,
  Divider,
  Card,
  H5
} from "@blueprintjs/core";

import {
  ICompareEntry,
  StringComparisonOperatorType,
  ValueType,
  NumberComparisonOperatorType
} from "./SpecEditorTypes";

interface ICompareEntryEditorProps {
  compareEntry?: ICompareEntry;
  okayButtonText: string;
  onOkayButtonClick(compareEntry: ICompareEntry): void;
  cancelButtonText: string;
  onCancelButtonClick?(): void;
}

// The state we keep here is not the outcome we want. We have attached several additional
// properties to that. This is true for RatioEntryEditor, other XxxxEntryViewers too.
const initialState = {
  payloadPath: "",
  fieldPath: "",
  type: ValueType.TEXT,
  operator: StringComparisonOperatorType.EQUALS,
  textValue: "",
  numberValue: 0
};

export default class CompareEntryEditor extends React.Component<
  ICompareEntryEditorProps,
  ICompareEntry
> {
  constructor(props) {
    super(props);

    this.state = props.compareEntry || initialState;
  }

  onOkayButtonClick = () => {
    this.props.onOkayButtonClick(this.state);
  };

  onCancelButtonClick = () => {
    if (!this.props.onOkayButtonClick) {
      this.setState(initialState);
    } else {
      this.props.onCancelButtonClick();
    }
  };

  onPayloadPathChange = event => {
    this.setState({
      payloadPath: event.target.value
    });
  };

  onFieldPathChange = event => {
    this.setState({
      fieldPath: event.target.value
    });
  };

  onTypeChange = event => {
    if (event.target.innerText.toUpperCase() === ValueType.TEXT) {
      this.setState({
        type: ValueType.TEXT,
        operator: StringComparisonOperatorType.EQUALS
      });
    } else {
      this.setState({
        type: ValueType.NUMBER,
        operator: NumberComparisonOperatorType.EQUAL
      });
    }
  };

  onOperatorChange = event => {
    this.setState({ operator: event.currentTarget.value });
  };

  onTextValueChange = event => {
    this.setState({
      textValue: event.target.value
    });
  };

  onNumberValueChange = valueAsNumber => {
    this.setState({
      numberValue: valueAsNumber
    });
  };

  render() {
    return (
      <Card>
        <H5>Add New Entry</H5>

        <FormGroup
          helperText="JSONPath expression to the payload"
          label="Payload path"
          labelFor="edit-compare-payload-path"
        >
          <InputGroup
            id="edit-compare-payload-path"
            placeholder="$.creation_process"
            onChange={this.onPayloadPathChange}
            value={this.state.payloadPath}
          />
        </FormGroup>

        <FormGroup
          helperText="JSONPath expression to the field to compare"
          label="Field path"
          labelFor="edit-compare-field-path"
        >
          <InputGroup
            id="edit-compare-field-path"
            placeholder={
              this.state.type === ValueType.TEXT
                ? "$.output.type"
                : "$.input[?(@.type == milk)].qty"
            }
            onChange={this.onFieldPathChange}
            value={this.state.fieldPath}
          />
        </FormGroup>

        <FormGroup
          helperText="Type of the value and value to compare"
          label="Compare to:"
          labelFor="edit-compare-value-info"
        >
          <ControlGroup id="edit-compare-value-info">
            <ButtonGroup onClick={this.onTypeChange}>
              <Button active={this.state.type === ValueType.TEXT}>Text</Button>
              <Button active={this.state.type === ValueType.NUMBER}>
                Number
              </Button>
            </ButtonGroup>
            <HTMLSelect
              id="edit-compare-value-type"
              options={
                this.state.type === ValueType.TEXT
                  ? Object.keys(StringComparisonOperatorType).map(
                      k => StringComparisonOperatorType[k]
                    )
                  : Object.keys(NumberComparisonOperatorType).map(
                      k => NumberComparisonOperatorType[k]
                    )
              }
              onChange={this.onOperatorChange}
              value={this.state.operator}
            />
            {this.state.type === ValueType.TEXT ? (
              <InputGroup
                id="edit-compare-text-value"
                placeholder="Cheese"
                onChange={this.onTextValueChange}
                value={this.state.textValue}
              />
            ) : (
              <NumericInput
                id="edit-compare-number-value"
                placeholder="10"
                onValueChange={this.onNumberValueChange}
                min={0}
                value={this.state.numberValue}
              />
            )}
          </ControlGroup>
        </FormGroup>

        <Divider />

        <Callout intent="primary" title="Expression preview">
          {`${this.state.fieldPath} ${this.state.operator} ${
            this.state.type === ValueType.NUMBER
              ? this.state.numberValue
              : this.state.textValue
          }`}
        </Callout>

        <ButtonGroup>
          <Button intent="none" onClick={this.onCancelButtonClick}>
            {this.props.cancelButtonText}
          </Button>
          <Button intent="primary" onClick={this.onOkayButtonClick}>
            {this.props.okayButtonText}
          </Button>
        </ButtonGroup>
      </Card>
    );
  }
}
