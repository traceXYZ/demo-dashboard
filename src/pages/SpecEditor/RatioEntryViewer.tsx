import * as React from "react";
import { Card, H5, ButtonGroup, Button } from "@blueprintjs/core";
import { IRatioEntry } from "./SpecEditorTypes";

interface IRatioEntryViewerProps {
  id: number;
  ratioEntry: IRatioEntry;
  editButtonText: string;
  onEditButtonClick(IRatioEntry): void;
  deleteButtonText: string;
  onDeleteButtonClick(number): void;
}

export default class RatioEntryViewer extends React.Component<
  IRatioEntryViewerProps
> {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      payloadPath,
      fieldPath1,
      fieldPath2,
      ratio,
      tolerance
    } = this.props.ratioEntry;
    return (
      <Card>
        <H5>Entry {this.props.id}</H5>

        <p>
          Payload Path: {payloadPath}
          <br />
          Field Path 1: {fieldPath1}
          <br />
          Field Path 2: {fieldPath2}
          <br />
          Ratio: {ratio}
          <br />
          Tolerance: {tolerance}
          <br />
        </p>

        <ButtonGroup>
          <Button
            intent="none"
            onClick={() => this.props.onEditButtonClick(this.props.ratioEntry)}
          >
            {this.props.editButtonText}
          </Button>
          <Button
            intent="warning"
            onClick={() => this.props.onDeleteButtonClick(this.props.id)}
          >
            {this.props.deleteButtonText}
          </Button>
        </ButtonGroup>
      </Card>
    );
  }
}
