import * as React from "react";

import CompareEntryViewer from "./CompareEntryViewer";
import CompareEntryEditor from "./CompareEntryEditor";
import { ICompareEntry } from "./SpecEditorTypes";

interface ICompareEntryPanelProps {
  entries: Array<ICompareEntry>;
  onEntriesChange(compareEntries: Array<ICompareEntry>): void;
}

export default class CompareEntryPanel extends React.Component<
  ICompareEntryPanelProps
> {
  constructor(props) {
    super(props);
  }

  onClickEditEntry = () => {};

  onClickDeleteEntry = (id: number) => {
    this.props.onEntriesChange(
      this.props.entries.filter((entry, i) => i !== id)
    );
  };

  onClickAddEntry = (entry: ICompareEntry) => {
    this.props.onEntriesChange([...this.props.entries, entry]);
  };

  render() {
    return (
      <>
        {this.props.entries.map((entry, i) => (
          <CompareEntryViewer
            compareEntry={entry}
            editButtonText="Edit"
            onEditButtonClick={this.onClickEditEntry}
            deleteButtonText="Delete"
            onDeleteButtonClick={this.onClickDeleteEntry}
            key={i}
            id={i}
          />
        ))}

        <CompareEntryEditor
          okayButtonText="Add"
          onOkayButtonClick={this.onClickAddEntry}
          cancelButtonText="Clear"
        />
      </>
    );
  }
}
