import * as React from "react";
import {
  InputGroup,
  FormGroup,
  ButtonGroup,
  Button,
  Card,
  H5
} from "@blueprintjs/core";

import { IQuotaEntry } from "./SpecEditorTypes";

interface IQuotaEntryEditorProps {
  QuotaEntry?: IQuotaEntry;
  okayButtonText: string;
  onOkayButtonClick(QuotaEntry: IQuotaEntry): void;
  cancelButtonText: string;
  onCancelButtonClick?(): void;
}

// The state we keep here is not the outcome we want. We have attached several additional
// properties to that. This is true for RatioEntryEditor, other XxxxEntryViewers too.
const initialState = {
  payloadPath: "",
  fieldPath: ""
};

export default class QuotaEntryEditor extends React.Component<
  IQuotaEntryEditorProps,
  IQuotaEntry
> {
  constructor(props) {
    super(props);

    this.state = props.QuotaEntry || initialState;
  }

  onOkayButtonClick = () => {
    this.props.onOkayButtonClick(this.state);
  };

  onCancelButtonClick = () => {
    if (!this.props.onOkayButtonClick) {
      this.setState(initialState);
    } else {
      this.props.onCancelButtonClick();
    }
  };

  onPayloadPathChange = event => {
    this.setState({
      payloadPath: event.target.value
    });
  };

  onFieldPathChange = event => {
    this.setState({
      fieldPath: event.target.value
    });
  };

  render() {
    return (
      <Card>
        <H5>Add New Entry</H5>

        <FormGroup
          helperText="JSONPath expression to the payload"
          label="Payload path"
          labelFor="edit-Quota-payload-path"
        >
          <InputGroup
            id="edit-Quota-payload-path"
            placeholder="$.created_item"
            onChange={this.onPayloadPathChange}
            value={this.state.payloadPath}
          />
        </FormGroup>

        <FormGroup
          helperText="JSONPath expression to find the quota field in user items"
          label="Field path"
          labelFor="edit-quota-field-path"
        >
          <InputGroup
            id="edit-quota-field-path"
            placeholder="$.qty"
            onChange={this.onFieldPathChange}
            value={this.state.fieldPath}
          />
        </FormGroup>

        <ButtonGroup>
          <Button intent="none" onClick={this.onCancelButtonClick}>
            {this.props.cancelButtonText}
          </Button>
          <Button intent="primary" onClick={this.onOkayButtonClick}>
            {this.props.okayButtonText}
          </Button>
        </ButtonGroup>
      </Card>
    );
  }
}
