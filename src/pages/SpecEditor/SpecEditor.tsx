import * as React from "react";
import {
  Tabs,
  Tab,
  Button,
  Classes,
  Dialog,
  ProgressBar,
  Callout,
  FormGroup,
  InputGroup
} from "@blueprintjs/core";

import CompareEntryPanel from "./CompareEntryPanel";
import RatioEntryPanel from "./RatioEntryPanel";
import QuotaEntryPanel from "./QuotaEntryPanel";
import {
  ICompareEntry,
  IQuotaEntry,
  IRatioEntry,
  ValueType
} from "./SpecEditorTypes";
import UserSelector from "../../components/UserSelector";
import { User } from "../MultiLogin/MultiLoginTypes";

interface ISpecEditorPageProps {
  onSendClick: (value: string, name: string, token: string) => void;

  result: string;
  loading: boolean;
  error: string;
}

interface IAugmentedSpecEditorPageProps extends ISpecEditorPageProps {
  users: Array<User>;
}

interface ISpecEditorPageState {
  user: User;
  compare: Array<ICompareEntry>;
  ratio: Array<IRatioEntry>;
  quota: Array<IQuotaEntry>;
  selectedTabId: string;
  isJsonVisible: boolean;
  specName: string;
}

const stripCompareEntry = (compareEntry: ICompareEntry) => ({
  payload_path: compareEntry.payloadPath,
  field_path: compareEntry.fieldPath,
  op: compareEntry.operator,
  val:
    compareEntry.type === ValueType.TEXT
      ? compareEntry.textValue
      : compareEntry.numberValue
});

const stripRatioEntry = (ratioEntry: IRatioEntry) => ({
  payload_path: ratioEntry.payloadPath,
  field_paths: [ratioEntry.fieldPath1, ratioEntry.fieldPath2],
  ratio: ratioEntry.ratio,
  tolerance: ratioEntry.tolerance
});

const stripQuotaEntry = (quotaEntry: IQuotaEntry) => ({
  payload_path: quotaEntry.payloadPath,
  field_path: quotaEntry.fieldPath
});

export default class SpecEditorPage extends React.Component<
  IAugmentedSpecEditorPageProps,
  ISpecEditorPageState
> {
  constructor(props) {
    super(props);

    this.state = {
      user: props.users && props.users[0],
      selectedTabId: "compare",
      compare: [],
      ratio: [],
      quota: [],
      isJsonVisible: false,
      specName: ""
    };
  }

  onUserSelect = (user: User) => {
    this.setState({ user });
  };

  onTabChange = tabId => {
    this.setState({ selectedTabId: tabId });
  };

  onViewJsonToggle = () => {
    this.setState({ isJsonVisible: !this.state.isJsonVisible });
  };

  onCompareEntriesChange = (compareEntries: Array<ICompareEntry>) => {
    this.setState({ compare: compareEntries });
  };

  onRatioEntriesChange = (ratioEntries: Array<IRatioEntry>) => {
    this.setState({ ratio: ratioEntries });
  };

  onQuotaEntriesChange = (quotaEntries: Array<IQuotaEntry>) => {
    this.setState({ quota: quotaEntries });
  };

  onSpecNameChange = event => {
    this.setState({ specName: event.target.value });
  };

  onSendClick = () => {
    this.props.onSendClick(
      JSON.stringify({
        compare: this.state.compare.map(stripCompareEntry),
        ratio: this.state.ratio.map(stripRatioEntry),
        quota: this.state.quota.map(stripQuotaEntry)
      }),
      this.state.specName,
      this.state.user.token
    );
  };

  render() {
    return (
      <div>
        <UserSelector
          onUserSelect={this.onUserSelect}
          users={this.props.users}
          selectedUser={this.state.user}
        />

        <Tabs
          onChange={this.onTabChange}
          selectedTabId={this.state.selectedTabId}
        >
          <Tab
            id="compare"
            title="Compare"
            panel={
              <CompareEntryPanel
                entries={this.state.compare}
                onEntriesChange={this.onCompareEntriesChange}
              />
            }
          />

          <Tab
            id="ratio"
            title="Ratio"
            panel={
              <RatioEntryPanel
                entries={this.state.ratio}
                onEntriesChange={this.onRatioEntriesChange}
              />
            }
          />

          <Tab
            id="quota"
            title="Quota"
            panel={
              <QuotaEntryPanel
                entries={this.state.quota}
                onEntriesChange={this.onQuotaEntriesChange}
              />
            }
          />
        </Tabs>

        <Button intent="success" onClick={this.onViewJsonToggle}>
          View JSON
        </Button>

        <Dialog
          title="Generated Specification"
          isOpen={this.state.isJsonVisible}
          onClose={this.onViewJsonToggle}
          isCloseButtonShown={!this.props.loading}
          canEscapeKeyClose={!this.props.loading}
          canOutsideClickClose={!this.props.loading}
        >
          <div className={Classes.DIALOG_BODY}>
            <FormGroup
              helperText="How do you identify the specification"
              label="Specification name"
              labelFor="spec-name-input"
              labelInfo="(required)"
            >
              <InputGroup
                id="spec-name-input"
                placeholder="SOME_SPECIFICATION"
                value={this.state.specName}
                onChange={this.onSpecNameChange}
              />
            </FormGroup>
            <pre>
              {JSON.stringify(
                {
                  compare: this.state.compare.map(stripCompareEntry),
                  ratio: this.state.ratio.map(stripRatioEntry),
                  quota: this.state.quota.map(stripQuotaEntry)
                },
                null,
                2
              )}
            </pre>
          </div>

          <div className={Classes.DIALOG_FOOTER}>
            <ProgressBar
              intent="primary"
              animate={this.props.loading}
              stripes={this.props.loading}
            />

            {this.props.error && (
              <Callout intent="danger">{this.props.error}</Callout>
            )}

            {this.props.result && (
              <Callout intent="success">
                {JSON.parse(this.props.result).message}
              </Callout>
            )}

            <div className={Classes.DIALOG_FOOTER_ACTIONS}>
              <Button
                disabled={
                  this.props.loading ||
                  !this.state.specName ||
                  this.state.specName.length < 1
                }
                intent="primary"
                onClick={this.onSendClick}
              >
                Send
              </Button>
            </div>
          </div>
        </Dialog>
      </div>
    );
  }
}
