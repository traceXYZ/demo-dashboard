import {
  put,
  takeEvery,
  ForkEffect,
  PutEffect,
  call
} from "redux-saga/effects";
import {
  AddSpecificationRequestActionType,
  receiveAddSpecification,
  AddSpecificationResponseAction,
  AddSpecificationRequestAction
} from "./actions";
import { addSpecification } from "../../services/api";

export function* watchAddSpecificationAsync(): Iterator<ForkEffect> {
  yield takeEvery(AddSpecificationRequestActionType.ADD, addSpecificationAsync);
}

function* addSpecificationAsync(
  action: AddSpecificationRequestAction
): AsyncIterableIterator<PutEffect<AddSpecificationResponseAction>> {
  const json = yield call(
    addSpecification,
    JSON.stringify({
      name: action.payload.name,
      specification: action.payload.specification
    }),
    action.payload.token
  );
  yield put(receiveAddSpecification(json));
}
