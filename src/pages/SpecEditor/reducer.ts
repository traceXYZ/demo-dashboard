import {
  AddSpecificationRequestActionType,
  AddSpecificationResponseActionType,
  AddSpecificationAction
} from "./actions";

export interface IAddSpecificationState {
  loading: boolean;
  result: string;
  error: string;
}

const defaultState: IAddSpecificationState = {
  loading: false,
  result: "",
  error: ""
};

export function specEditor(
  state = defaultState,
  action: AddSpecificationAction
): IAddSpecificationState {
  switch (action.type) {
    case AddSpecificationRequestActionType.ADD:
      return { loading: true, result: "", error: "" };

    case AddSpecificationResponseActionType.ADD:
      if (action.error) {
        return { loading: false, error: action.error, result: "" };
      }
      if (action.result) {
        const resultJson = JSON.parse(action.result);
        if (resultJson.error) {
          return {
            loading: false,
            result: "",
            error: resultJson.error
          };
        }
      }
      return { loading: false, result: action.result, error: "" };
    default:
      return state;
  }
}
