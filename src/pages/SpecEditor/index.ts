import { connect } from "react-redux";

import SpecEditor from "./SpecEditor";
import { sendAddSpecification } from "./actions";
import { IAddSpecificationState } from "./reducer";
import { User } from "../MultiLogin/MultiLoginTypes";

const mapStateToProps = (state: {
  specEditor: IAddSpecificationState;
  logins: { users: Array<User> };
}) => {
  return { ...state.specEditor, users: state.logins.users };
};

const mapDispatchToProps = dispatch => {
  return {
    onSendClick: (specification: string, name: string, token: string) => {
      dispatch(sendAddSpecification(specification, name, token));
    }
  };
};

const SpecEditorPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(SpecEditor);

export default SpecEditorPage;
