import * as React from "react";

import RatioEntryViewer from "./RatioEntryViewer";
import RatioEntryEditor from "./RatioEntryEditor";
import { IRatioEntry } from "./SpecEditorTypes";

interface IRatioEntryPanelProps {
  entries: Array<IRatioEntry>;
  onEntriesChange(ratioEntries: Array<IRatioEntry>): void;
}

export default class RatioEntryPanel extends React.Component<
  IRatioEntryPanelProps
> {
  constructor(props) {
    super(props);

    this.state = { entries: props.entries };
  }

  onClickEditEntry = () => {};

  onClickDeleteEntry = (id: number) => {
    this.props.onEntriesChange(
      this.props.entries.filter((entry, i) => i !== id)
    );
  };

  onClickAddEntry = (entry: IRatioEntry) => {
    this.props.onEntriesChange([...this.props.entries, entry]);
  };

  render() {
    return (
      <>
        {this.props.entries.map((entry, i) => (
          <RatioEntryViewer
            ratioEntry={entry}
            editButtonText="Edit"
            onEditButtonClick={this.onClickEditEntry}
            deleteButtonText="Delete"
            onDeleteButtonClick={this.onClickDeleteEntry}
            key={i}
            id={i}
          />
        ))}

        <RatioEntryEditor
          okayButtonText="Add"
          onOkayButtonClick={this.onClickAddEntry}
          cancelButtonText="Clear"
        />
      </>
    );
  }
}
