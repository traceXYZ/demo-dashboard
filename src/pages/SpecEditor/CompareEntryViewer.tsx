import * as React from "react";
import { Card, H5, Button, ButtonGroup } from "@blueprintjs/core";

import { ICompareEntry, ValueType } from "./SpecEditorTypes";

interface ICompareEntryViewerProps {
  id: number;
  compareEntry: ICompareEntry;
  editButtonText: string;
  onEditButtonClick(ICompareEntry): void;
  deleteButtonText: string;
  onDeleteButtonClick(number): void;
}

export default class CompareEntryViewer extends React.Component<
  ICompareEntryViewerProps
> {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      fieldPath,
      numberValue,
      operator,
      payloadPath,
      textValue,
      type
    } = this.props.compareEntry;

    return (
      <Card>
        <H5>Entry {this.props.id}</H5>

        <p>
          Payload Path: {payloadPath}
          <br />
          Field Path: {fieldPath}
          <br />
          Operator: {operator}
          <br />
          Value: {type === ValueType.TEXT ? textValue : numberValue}
          <br />
        </p>

        <ButtonGroup>
          <Button
            intent="none"
            onClick={() =>
              this.props.onEditButtonClick(this.props.compareEntry)
            }
          >
            {this.props.editButtonText}
          </Button>
          <Button
            intent="warning"
            onClick={() => this.props.onDeleteButtonClick(this.props.id)}
          >
            {this.props.deleteButtonText}
          </Button>
        </ButtonGroup>
      </Card>
    );
  }
}
