import * as React from "react";
import { Card, H5, Button, ButtonGroup } from "@blueprintjs/core";

import { IQuotaEntry } from "./SpecEditorTypes";

interface IQuotaEntryViewerProps {
  id: number;
  QuotaEntry: IQuotaEntry;
  editButtonText: string;
  onEditButtonClick(IQuotaEntry): void;
  deleteButtonText: string;
  onDeleteButtonClick(number): void;
}

export default class QuotaEntryViewer extends React.Component<
  IQuotaEntryViewerProps
> {
  constructor(props) {
    super(props);
  }

  render() {
    const { fieldPath, payloadPath } = this.props.QuotaEntry;

    return (
      <Card>
        <H5>Entry {this.props.id}</H5>

        <p>
          Payload Path: {payloadPath}
          <br />
          Field Path: {fieldPath}
          <br />
        </p>

        <ButtonGroup>
          <Button
            intent="none"
            onClick={() => this.props.onEditButtonClick(this.props.QuotaEntry)}
          >
            {this.props.editButtonText}
          </Button>
          <Button
            intent="warning"
            onClick={() => this.props.onDeleteButtonClick(this.props.id)}
          >
            {this.props.deleteButtonText}
          </Button>
        </ButtonGroup>
      </Card>
    );
  }
}
