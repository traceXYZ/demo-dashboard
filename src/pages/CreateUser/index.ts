import { connect } from "react-redux";

import CreateUser from "./CreateUser";
import { sendCreateUser } from "./actions";
import { ICreateUserState } from "./reducer";

const mapStateToProps = (state: { createUser: ICreateUserState }) => {
  return state.createUser;
};

const mapDispatchToProps = dispatch => {
  return {
    onAddClick: payload => {
      dispatch(sendCreateUser(payload));
    }
  };
};

const CreateUserPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateUser);

export default CreateUserPage;
