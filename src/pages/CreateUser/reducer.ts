import {
  CreateUserRequestActionType,
  CreateUserResponseActionType,
  CreateUserAction
} from "./actions";

export interface ICreateUserState {
  loading: boolean;
  result: string;
  error: string;
}

const defaultState: ICreateUserState = {
  loading: false,
  result: "",
  error: ""
};

export function createUser(
  state = defaultState,
  action: CreateUserAction
): ICreateUserState {
  switch (action.type) {
    case CreateUserRequestActionType.CREATE:
      return { loading: true, result: "", error: "" };

    case CreateUserResponseActionType.CREATE:
      if (action.error) {
        return { loading: false, error: action.error, result: "" };
      }
      return { loading: false, result: action.result, error: "" };
    default:
      return state;
  }
}
