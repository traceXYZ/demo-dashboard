import * as React from "react";
import {
  FormGroup,
  InputGroup,
  Checkbox,
  Button,
  ProgressBar
} from "@blueprintjs/core";

interface ICreateUserPageProps {
  onAddClick: (value: ICreateUserPageState) => void;
  loading: boolean;
}

interface ICreateUserPageState {
  username: string;
  password: string;
  isAdmin: boolean;
}

export default class CreateUserPage extends React.Component<
  ICreateUserPageProps,
  ICreateUserPageState
> {
  constructor(props) {
    super(props);

    this.state = { username: "", password: "", isAdmin: false };
  }

  onUsernameChange = event => {
    this.setState({ username: event.target.value });
  };

  onPasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  onAdminToggle = () => {
    this.setState({ isAdmin: !this.state.isAdmin });
  };

  onAddClick = () => {
    this.props.onAddClick(this.state);
  };

  render() {
    return (
      <>
        <FormGroup
          helperText="Uniquely identify the user"
          label="Username"
          labelFor="create-user-username"
        >
          <InputGroup
            id="create-user-username"
            placeholder={"johndoe"}
            onChange={this.onUsernameChange}
            value={this.state.username}
          />
        </FormGroup>
        <FormGroup
          helperText="Enter something that you remember well, but others don't"
          label="Password"
          labelFor="create-user-password"
        >
          <InputGroup
            type="password"
            id="create-user-password"
            placeholder={"correct horse battery staple"}
            onChange={this.onPasswordChange}
            value={this.state.password}
          />
        </FormGroup>

        <Checkbox
          checked={this.state.isAdmin}
          label="Administrator"
          onChange={this.onAdminToggle}
        />

        <ProgressBar
          animate={this.props.loading}
          stripes={this.props.loading}
          intent="primary"
        />

        <Button intent="success" onClick={this.onAddClick}>
          Add User
        </Button>
      </>
    );
  }
}
