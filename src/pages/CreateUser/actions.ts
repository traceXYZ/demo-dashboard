type CreateUserPayload = {
  username: string;
  password: string;
  isAdmin: boolean;
};

export enum CreateUserRequestActionType {
  CREATE = "USER_CREATE_REQUEST_SEND"
}

export enum CreateUserResponseActionType {
  CREATE = "USER_CREATE_RESPONSE_RECEIVE"
}

export type CreateUserRequestAction = {
  type: CreateUserRequestActionType;
  payload: CreateUserPayload;
};

export type CreateUserResponseAction = {
  type: CreateUserResponseActionType;
  result?: string;
  error?: string;
};

export type CreateUserAction =
  | CreateUserRequestAction
  | CreateUserResponseAction;

export function sendCreateUser(
  payload: CreateUserPayload
): CreateUserRequestAction {
  return {
    type: CreateUserRequestActionType.CREATE,
    payload
  };
}

export function receiveCreateUser(result: string): CreateUserResponseAction {
  return {
    type: CreateUserResponseActionType.CREATE,
    result
  };
}
