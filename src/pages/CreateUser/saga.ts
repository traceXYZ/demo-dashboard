import {
  put,
  takeEvery,
  ForkEffect,
  PutEffect,
  call
} from "redux-saga/effects";
import {
  CreateUserRequestActionType,
  receiveCreateUser,
  CreateUserResponseAction,
  CreateUserRequestAction
} from "./actions";
import { createUser } from "../../services/api";

export function* watchCreateUserAsync(): Iterator<ForkEffect> {
  yield takeEvery(CreateUserRequestActionType.CREATE, createUserAsync);
}

function* createUserAsync(
  action: CreateUserRequestAction
): AsyncIterableIterator<PutEffect<CreateUserResponseAction>> {
  let json = yield call(createUser, JSON.stringify(action.payload));
  yield put(receiveCreateUser(json));
}
