// Action types

export enum RequestActionType {
  INVOKE = "INVOKE_REQUEST_SEND",
  QUERY = "QUERY_REQUEST_SEND"
}

export enum ResponseActionType {
  INVOKE = "INVOKE_RESPONSE_RECEIVE",
  QUERY = "QUERY_RESPONSE_RECEIVE"
}

export type RequestAction = {
  type: RequestActionType;
  payload: { json: string; chaincodeId: string; fcn: string };
  token: string;
};

export type ResponseAction = {
  type: ResponseActionType;
  result?: string;
  error?: string;
};

export type EditorAction = RequestAction | ResponseAction;

// Action creators

export function sendInvoke(
  {
    json,
    chaincodeId,
    fcn
  }: { json: string; chaincodeId: string; fcn: string },
  token: string
): RequestAction {
  return {
    type: RequestActionType.INVOKE,
    payload: { json, chaincodeId, fcn },
    token
  };
}

export function sendQuery(
  {
    json,
    chaincodeId,
    fcn
  }: { json: string; chaincodeId: string; fcn: string },
  token: string
): RequestAction {
  return {
    type: RequestActionType.QUERY,
    payload: { json, chaincodeId, fcn },
    token
  };
}

export function receiveInvoke(result: string, error: string): ResponseAction {
  return {
    type: ResponseActionType.INVOKE,
    result,
    error
  };
}

export function receiveQuery(result: string, error: string): ResponseAction {
  return {
    type: ResponseActionType.QUERY,
    result,
    error
  };
}
