import { connect } from "react-redux";

import Editor from "./Editor";
import { sendInvoke, sendQuery } from "./actions";
import { IEditorState } from "./reducer";
import { User } from "../MultiLogin/MultiLoginTypes";

// Selector - derive/pluck data to be used by presentational component from state
const mapStateToProps = (state: {
  editor: IEditorState;
  logins: { users: Array<User> };
}) => {
  return { ...state.editor, users: state.logins.users };
};

// Specify functions to be called by the presentational component
// The functions dispatch actions to be used by the reducer
const mapDispatchToProps = dispatch => {
  return {
    onClickInvoke: (
      json: string,
      chaincodeId: string,
      fcn: string,
      token: string
    ) => {
      dispatch(sendInvoke({ json, chaincodeId, fcn }, token));
    },

    onClickQuery: (
      json: string,
      chaincodeId: string,
      fcn: string,
      token: string
    ) => {
      dispatch(sendQuery({ json, chaincodeId, fcn }, token));
    }
  };
};

// Connect presentation component to the state
const EditorPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(Editor);

export default EditorPage;
