import {
  put,
  takeEvery,
  ForkEffect,
  PutEffect,
  call
} from "redux-saga/effects";
import {
  RequestActionType,
  receiveInvoke,
  receiveQuery,
  ResponseAction,
  RequestAction
} from "./actions";
import { invoke, query } from "../../services/api";

// Watch for specific action types

export function* watchSendInvokeAsync(): Iterator<ForkEffect> {
  yield takeEvery(RequestActionType.INVOKE, sendInvokeAsync);
}

export function* watchSendQueryAsync(): Iterator<ForkEffect> {
  yield takeEvery(RequestActionType.QUERY, sendQueryAsync);
}

// Execute actions asynchronously and dispatch in case of new results

function* sendInvokeAsync(
  action: RequestAction
): AsyncIterableIterator<PutEffect<ResponseAction>> {
  try {
    const request = JSON.stringify(action.payload);
    let json = yield call(invoke, request, action.token);
    yield put(receiveInvoke(json, null));
  } catch (error) {
    yield put(receiveInvoke(null, error.message));
  }
}

function* sendQueryAsync(
  action: RequestAction
): AsyncIterableIterator<PutEffect<ResponseAction>> {
  try {
    const request = JSON.stringify(action.payload);
    let json = yield call(query, request, action.token);
    yield put(receiveQuery(json, null));
  } catch (error) {
    yield put(receiveQuery(null, error.message));
  }
}
