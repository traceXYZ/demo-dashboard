import { ResponseActionType, RequestActionType, EditorAction } from "./actions";

export interface IEditorState {
  loading: boolean;
  result: string;
  error: string;
}

const defaultState: IEditorState = {
  loading: false,
  result: "",
  error: ""
};

// Derive next state using actions+payload and current state
export function editor(
  state = defaultState,
  action: EditorAction
): IEditorState {
  switch (action.type) {
    case RequestActionType.INVOKE: // Captured by saga
      return { loading: true, result: "", error: "" };
    case RequestActionType.QUERY: // Captured by saga
      return { loading: true, result: "", error: "" };

    case ResponseActionType.INVOKE:
      if (action.error) {
        return { loading: false, error: action.error, result: "" };
      }
      return { loading: false, result: action.result, error: "" };

    case ResponseActionType.QUERY:
      if (action.error) {
        return { loading: false, error: action.error, result: "" };
      }
      return { loading: false, result: action.result, error: "" };

    default:
      return state;
  }
}
