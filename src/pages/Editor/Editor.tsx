import * as React from "react";
import {
  Button,
  ButtonGroup,
  ProgressBar,
  Callout,
  FormGroup,
  InputGroup,
  H5
} from "@blueprintjs/core";

import JsonEditor from "../../components/JsonEditor";
import UserSelector from "../../components/UserSelector";
import { User } from "../MultiLogin/MultiLoginTypes";
import { Row } from "../../components/Layout/Flex";

interface IEditorPageProps {
  onClickInvoke: (
    value: string,
    chaincodeId: string,
    fcn: string,
    token: string
  ) => void;
  onClickQuery: (
    value: string,
    chaincodeId: string,
    fcn: string,
    token: string
  ) => void;

  result: string;
  error: string;
  loading: boolean;
}

interface IAugmentedEditorPageProps extends IEditorPageProps {
  users: Array<User>;
}

interface IEditorPageState {
  json: string;
  user: User;
  chaincodeId: string;
  fcn: string;
}

export default class EditorPage extends React.Component<
  IAugmentedEditorPageProps,
  IEditorPageState
> {
  constructor(props) {
    super(props);

    this.state = {
      json: "",
      user: props.users && props.users[0],
      chaincodeId: "",
      fcn: ""
    };
  }

  onUserSelect = (user: User) => {
    this.setState({ user });
  };

  onChaincodeIdChange = event => {
    this.setState({ chaincodeId: event.target.value });
  };

  onFcnChange = event => {
    this.setState({ fcn: event.target.value });
  };

  onJsonChange = (json: string) => {
    this.setState({ json });
  };

  onInvokeClick = () => {
    this.props.onClickInvoke(
      this.state.json,
      this.state.chaincodeId,
      this.state.fcn,
      this.state.user.token
    );
  };

  onQueryClick = () => {
    this.props.onClickQuery(
      this.state.json,
      this.state.chaincodeId,
      this.state.fcn,
      this.state.user.token
    );
  };

  render() {
    return (
      <div>
        <UserSelector
          onUserSelect={this.onUserSelect}
          users={this.props.users}
          selectedUser={this.state.user}
        />

        <Row>
          <FormGroup
            style={{ flexGrow: 1 }}
            helperText="Enter the name of the chaincode assigned to you"
            label="Chaincode Id"
            labelFor="chaincode-id-input"
            labelInfo="(required)"
          >
            <InputGroup
              id="chaincode-id-input"
              placeholder="milkproducer_cc"
              value={this.state.chaincodeId}
              onChange={this.onChaincodeIdChange}
            />
          </FormGroup>

          <FormGroup
            style={{ flexGrow: 1 }}
            helperText="Enter the name of the function assigned to you"
            label="Function"
            labelFor="chaincode-function-input"
            labelInfo="(required)"
          >
            <InputGroup
              id="chaincode-function-input"
              placeholder="produce_milk"
              value={this.state.fcn}
              onChange={this.onFcnChange}
            />
          </FormGroup>
        </Row>

        <H5>Argument</H5>
        <JsonEditor onChange={this.onJsonChange} height="400px" />

        <ButtonGroup>
          <Button onClick={this.onInvokeClick}>Invoke</Button>
          <Button onClick={this.onQueryClick}>Query</Button>
        </ButtonGroup>

        <ProgressBar
          animate={this.props.loading}
          stripes={this.props.loading}
        />

        {this.props.error && (
          <Callout title="Failed" intent="danger">
            <p>{this.props.error}</p>
          </Callout>
        )}

        {this.props.result && (
          <Callout title="Success" intent="success">
            <p>{this.props.result}</p>
          </Callout>
        )}
      </div>
    );
  }
}
