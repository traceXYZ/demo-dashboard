import {
  put,
  takeEvery,
  ForkEffect,
  PutEffect,
  call
} from "redux-saga/effects";
import {
  TraceRequestActionType,
  receiveTraceStart,
  receiveTraceContinue,
  TraceResponseAction,
  TraceRequestAction
} from "./actions";
import { query } from "../../services/api";
import { GraphResult, ITraceStartDialogState, StorageItem } from "./TraceTypes";

// Watch for specific action types

export function* watchSendTraceStartAsync(): Iterator<ForkEffect> {
  yield takeEvery(TraceRequestActionType.TRACE_START, sendTraceStartAsync);
}

export function* watchSendTraceContinueAsync(): Iterator<ForkEffect> {
  yield takeEvery(
    TraceRequestActionType.TRACE_CONTINUE,
    sendTraceContinueAsync
  );
}
interface IQuery {
  chaincodeId: string;
  fcn: string;
  args: Array<string>;
  chainId: string;
}

interface IRetreiveRequest {
  item_id: string;
  relationship_type: string;
  retrieve_type: string;
  walk_path: Array<number>;
}

function transformQuery(query: ITraceStartDialogState): IRetreiveRequest {
  return {
    item_id: query.itemId,
    relationship_type: query.relationshipType,
    retrieve_type: query.traceDirection,
    walk_path: []
  };
}

// TODO Extract as a service
function createRequest(
  request: IRetreiveRequest | string,
  fcn: string
): IQuery {
  return {
    args: [typeof request === "string" ? request : JSON.stringify(request)],
    // TODO Fetch from config
    chaincodeId: "storage_cc",
    chainId: "cheeseproduction",
    fcn
  };
}

const decodePayload = item => {
  return (
    (item &&
      item.payload && { ...item, payload: JSON.parse(atob(item.payload)) }) ||
    {}
  );
};

// const buildStorageItemsDict = storageItems => {
//   const storageItemsDict = {};

//   for (let i = 0; i < storageItems.length; i++) {
//     const storageItem = storageItems[i];

//     storageItemsDict[storageItem.id] = storageItem;
//   }

//   return storageItemsDict;
// };

const itemTypeForId = (id, data) => {
  const found = data.filter(it => it.id === id);
  const t = ((found && found[0]) || {}).payload;
  if (!t) {
    return "";
  }
  return t.itemType || t.boxType || t.processType;
};

const assignLabel = (node, storageItems) => {
  return { ...node, label: itemTypeForId(node.id, storageItems) };
};

const cleanGraphResult: (graphResult: GraphResult) => GraphResult = (
  graphResult: GraphResult
) => {
  const grClean = { ...graphResult };

  grClean.storage_items = grClean.storage_items || [];
  grClean.storage_items = grClean.storage_items.map(decodePayload);

  grClean.edges = grClean.edges || [];

  grClean.nodes = grClean.nodes || [];
  grClean.nodes = grClean.nodes.map(node =>
    assignLabel(node, grClean.storage_items)
  );
  return grClean;
};

// Execute actions asynchronously and dispatch in case of new results
function* sendTraceStartAsync(
  action: TraceRequestAction
): AsyncIterableIterator<PutEffect<TraceResponseAction>> {
  const first = createRequest(action.payload.query.itemId, "retrieve_direct");
  const rootObjResult = yield call(
    query,
    JSON.stringify(first),
    action.payload.token
  );
  const rootObj = JSON.parse(rootObjResult).storage_item as StorageItem;

  if (rootObj) {
    const queryArgs = createRequest(
      transformQuery(action.payload.query),
      "retrieve"
    );
    const graphResult = yield call(
      query,
      JSON.stringify(queryArgs),
      action.payload.token
    );

    const graph = cleanGraphResult(JSON.parse(graphResult) as GraphResult);
    graph.nodes.push({
      id: action.payload.query.itemId,
      type: "Unknown",
      enabled: true
    });
    graph.storage_items.push(decodePayload(rootObj));

    yield put(receiveTraceStart(graph));
  } else {
    yield put(receiveTraceStart(null));
  }
}

function* sendTraceContinueAsync(
  action: TraceRequestAction
): AsyncIterableIterator<PutEffect<TraceResponseAction>> {
  const queryArgs = createRequest(
    transformQuery(action.payload.query),
    "retrieve"
  );
  const graphResult = yield call(
    query,
    JSON.stringify(queryArgs),
    action.payload.token
  );

  const graph = cleanGraphResult(JSON.parse(graphResult) as GraphResult);
  yield put(receiveTraceContinue(graph));
}
