import * as React from "react";

import XGraph from "./XGraph";

const graphConfig = {
  automaticRearrangeAfterDropNode: true,
  collapsible: true,
  nodeHighlightBehavior: true,
  linkHightlightBehavior: true,
  node: {
    color: "DeepSkyBlue",
    highlightFontWeight: "bold",
    highlightFontColor: "red",
    labelProperty: "label"
  },
  link: {
    type: "CURVE_SMOOTH",
    highlightColor: "red"
  }
};

interface IGraphNode {
  svg: string;
  id: string;
  [x: string]: any;
}

interface IGraphLink {
  source: string;
  target: string;
}

interface IGraph {
  nodes: Array<IGraphNode>;
  links: Array<IGraphLink>;
}

interface IGraphViewProps {
  data: IGraph;

  onSelectNode: (id: string) => void;
}

interface IGraphViewState {
  height: number;
  width: number;
}

class GraphView extends React.Component<IGraphViewProps, IGraphViewState> {
  constructor(props) {
    super(props);

    this.state = {
      height: window.screen.height - 250,
      width: window.screen.width - 300
    };
  }

  onSelectNode = id => {
    this.props.onSelectNode(id);
  };

  render() {
    return (
      <>
        <XGraph
          id="trace-graph"
          config={{
            ...graphConfig,
            height: this.state.height,
            width: this.state.width
          }}
          data={this.props.data}
          onClickNode={this.onSelectNode}
        />
      </>
    );
  }
}

export default GraphView;
