import merge from "deepmerge";

import {
  TraceResponseActionType,
  TraceRequestActionType,
  TraceAction
} from "./actions";
import { GraphResult } from "./TraceTypes";

export interface ITraceState {
  loading: boolean;
  result: GraphResult;
  error: string;
}

const emptyGraph: GraphResult = {
  nodes: [],
  edges: [],
  message: "",
  status: "",
  storage_items: []
};

const defaultState: ITraceState = {
  loading: false,
  result: emptyGraph,
  error: ""
};

// Derive next state using actions+payload and current state
export function trace(state = defaultState, action: TraceAction): ITraceState {
  switch (action.type) {
    case TraceRequestActionType.TRACE_START: // Captured by saga
      return { loading: true, result: emptyGraph, error: "" };
    case TraceRequestActionType.TRACE_CONTINUE: // Captured by saga
      return {
        loading: false,
        error: "",
        result: merge(state.result, emptyGraph)
      };
    case TraceResponseActionType.TRACE_START:
      if (action.error) {
        return { loading: false, error: action.error, result: emptyGraph };
      }
      return { loading: false, result: action.result, error: "" };
    case TraceResponseActionType.TRACE_CONTINUE:
      if (action.error) {
        return {
          loading: false,
          error: action.error,
          result: merge(state.result, emptyGraph)
        };
      }

      const result = merge(state.result, action.result);
      return { loading: false, result, error: "" };
    default:
      return state;
  }
}
