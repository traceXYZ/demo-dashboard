export interface GraphNode {
  id: String;
  type: String;
  enabled: boolean;
}

export interface GraphEdge {
  type: String;
  state: String;
  source: String;
  destination: String;
  enabled: boolean;
}

export interface StorageItem {
  [x: string]: any;
}

export interface GraphResult {
  status: String;
  message: String;
  nodes: Array<GraphNode>;
  edges: Array<GraphEdge>;
  storage_items: Array<StorageItem>;
}

export interface ITraceStartDialogState {
  itemId: string;

  // TODO Represent `traceDirection` and `relationshipType` with enums
  // TODO and update how radio buttons to use them
  traceDirection: string;
  relationshipType: string;
}
