import * as React from "react";
import {
  Dialog,
  FormGroup,
  Classes,
  InputGroup,
  RadioGroup,
  Radio,
  ProgressBar,
  Callout,
  Button
} from "@blueprintjs/core";
import { ITraceStartDialogState } from "./TraceTypes";

interface ITraceStartDialogProps {
  onTraceClick: (args: ITraceStartDialogState) => void;
  onStartDialogToggle: () => void;

  startDialogVisible: boolean;
  loading: boolean;
  error: string;
  idLocked: boolean;
  selectedId?: string;
}

class TraceStartDialog extends React.Component<
  ITraceStartDialogProps,
  ITraceStartDialogState
> {
  constructor(props) {
    super(props);

    this.state = {
      itemId: this.props.selectedId || "",
      traceDirection: "All",
      relationshipType: "PublicTrace"
    };
  }

  onItemIdChange = event => {
    this.setState({ itemId: event.target.value });
  };

  onTraceDirectionChange = event => {
    this.setState({ traceDirection: event.target.value });
  };

  onRelationshipTypeChange = event => {
    this.setState({ relationshipType: event.target.value });
  };

  onTraceClick = () => {
    this.props.onTraceClick(this.state);
  };

  render() {
    return (
      <Dialog
        title="Trace an Item"
        isOpen={this.props.startDialogVisible}
        onClose={this.props.onStartDialogToggle}
        isCloseButtonShown={!this.props.loading}
        canEscapeKeyClose={!this.props.loading}
        canOutsideClickClose={!this.props.loading}
      >
        <div className={Classes.DIALOG_BODY}>
          <FormGroup
            helperText="Identifier of the object to trace"
            label="Item Id"
            labelFor="edit-trace-item-id"
          >
            <InputGroup
              id="edit-trace-item-id"
              disabled={this.props.idLocked}
              placeholder="442eea12-41db-4bc4-9459-7380b3bbf6c7a224cff5-fce3-40c4-8025-b37fafb72cea"
              onChange={this.onItemIdChange}
              value={this.state.itemId}
            />
          </FormGroup>

          <RadioGroup
            label="Trace Direction"
            onChange={this.onTraceDirectionChange}
            selectedValue={this.state.traceDirection}
          >
            <Radio label="Immediate History" value="ImmediateHistory" />
            <Radio label="All History" value="AllHistory" />
            <Radio label="Immediate Future" value="ImmediateFuture" />
            <Radio label="All Future" value="AllFuture" />
            <Radio label="Immediate History and Future" value="Immediate" />
            <Radio label="Everything" value="All" />
            <Radio label="Custom Walk" value="CustomWalk" />
          </RadioGroup>

          <RadioGroup
            label="Relationship Type"
            onChange={this.onRelationshipTypeChange}
            selectedValue={this.state.relationshipType}
          >
            <Radio label="Public Trace" value="PublicTrace" />
            <Radio label="Specification" value="Specification" />
            {/* <Radio label="Handler" value="Handler" />
            <Radio label="Owner" value="Owner" />
            <Radio label="Transport" value="Transport" />
            <Radio label="Transfer" value="Transfer" /> */}
            <Radio label="IoT Update" value="IOTUpdate" />
          </RadioGroup>
        </div>

        <div className={Classes.DIALOG_FOOTER}>
          <ProgressBar
            intent="primary"
            animate={this.props.loading}
            stripes={this.props.loading}
          />
          {this.props.error && (
            <Callout intent="danger">{this.props.error}</Callout>
          )}

          <div className={Classes.DIALOG_FOOTER_ACTIONS}>
            <Button
              disabled={this.props.loading}
              intent="primary"
              onClick={this.onTraceClick}
            >
              Trace
            </Button>
          </div>
        </div>
      </Dialog>
    );
  }
}

export default TraceStartDialog;
