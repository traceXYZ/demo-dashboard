import createRawMaterialProcess from "./CreateRawMaterialProcess.svg";
import manufactureProcess from "./ManufactureProcess.svg";
import rawMaterial from "./RawMaterial.svg";
import mergeProcessData from "./MergeProcessData.svg";
import splitProcessData from "./SplitProcessData.svg";
import divideProcess from "./DivideProcess.svg";
import transportProcess from "./TransportProcess.svg";
import item from "./Item.svg";
import container from "./Container.svg";
import sellProcess from "./SellProcess.svg";
import handlingRightsTransferProcess from "./HandlingRightsTransferProcess.svg";
import ownershipTransferProcess from "./OwnershipTransferProcess.svg";
import processUpdate from "./ProcessUpdate.svg";
import specification from "./specification.svg";

const svgLookup = {
  CreateRawMaterialProcess: createRawMaterialProcess,
  ManufactureProcess: manufactureProcess,
  RawMaterial: rawMaterial,
  MergeProcessData: mergeProcessData,
  SplitProcessData: splitProcessData,
  DivideProcess: divideProcess,
  TransportProcess: transportProcess,
  Item: item,
  Container: container,
  SellProcess: sellProcess,
  HandlingRightsTransferProcess: handlingRightsTransferProcess,
  OwnershipTransferProcess: ownershipTransferProcess,
  ProcessUpdate: processUpdate,
  Specification: specification
};

const injectNodeTypeIcons = node => {
  return { ...node, svg: svgLookup[node.type] };
};

export const graphToD3 = obj => {
  return {
    nodes: obj.nodes.map(injectNodeTypeIcons),
    links: obj.edges.map(({ source, destination: target }) => ({
      source,
      target
    }))
  };
};
