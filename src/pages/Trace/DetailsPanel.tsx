import * as React from "react";
import {
  Button,
  Card,
  Dialog,
  Classes,
  H5,
  ButtonGroup,
  Divider
} from "@blueprintjs/core";
import TraceStartDialog from "./TraceStartDialog";
import { ITraceStartDialogState } from "./TraceTypes";

interface IDetailsPanelProps {
  onTraceClick: (state: ITraceStartDialogState, contd: Boolean) => void;

  id: string;
  payload: string;
  error: string;
  loading: boolean;
}

interface IDetailsPanelState {
  startDialogVisible: boolean;
  continue: boolean;
  ownershipDialogVisible: boolean;
  handlerDialogVisible: boolean;
  imageDialogVisible: boolean;
}

const separateOwnership = payload => {
  const ownershipObj = JSON.parse(payload).ownership;
  const { public_key, challenge, payload_signature } = ownershipObj || {
    public_key: "N/A",
    challenge: "N/A",
    payload_signature: "N/A"
  };
  return (
    <>
      <H5>Public Key</H5> <pre>{public_key}</pre>
      <H5>Challenge</H5> <pre>{challenge}</pre>
      <H5>Payload Signature</H5> <pre>{payload_signature}</pre>
    </>
  );
};

const separateHandlerPublicKey = payload => {
  const handler = JSON.parse(payload).handler_public_key;
  return <pre>{JSON.stringify(handler, null, 2)}</pre>;
};

class DetailsPanel extends React.Component<
  IDetailsPanelProps,
  IDetailsPanelState
> {
  constructor(props) {
    super(props);

    this.state = {
      startDialogVisible: false,
      continue: true,
      ownershipDialogVisible: false,
      handlerDialogVisible: false,
      imageDialogVisible: false
    };
  }

  onOwnershipDialogToggle = () => {
    this.setState({
      ownershipDialogVisible: !this.state.ownershipDialogVisible
    });
  };

  onHandlerDialogToggle = () => {
    this.setState({
      handlerDialogVisible: !this.state.handlerDialogVisible
    });
  };

  onContinueDialogToggle = () => {
    this.setState({
      continue: true,
      startDialogVisible: !this.state.startDialogVisible
    });
  };

  onRestartDialogToggle = () => {
    this.setState({
      continue: false,
      startDialogVisible: !this.state.startDialogVisible
    });
  };

  onImageDialogToggle = () => {
    this.setState({ imageDialogVisible: !this.state.imageDialogVisible });
  };

  onTraceClick = (state: ITraceStartDialogState) => {
    this.props.onTraceClick(state, this.state.continue);
  };

  render() {
    return (
      <div style={{ width: 250 }}>
        <Card>
          <h5>Details</h5>
          <p>{this.props.id}</p>
          <pre>
            {this.props.payload &&
              JSON.stringify(JSON.parse(this.props.payload).payload, null, 2)}
          </pre>

          <TraceStartDialog
            selectedId={this.props.id}
            error={this.props.error}
            loading={this.props.loading}
            idLocked={false}
            startDialogVisible={this.state.startDialogVisible}
            onStartDialogToggle={this.onContinueDialogToggle}
            onTraceClick={this.onTraceClick}
          />

          <Dialog
            style={{ width: "75%" }}
            title="Ownership"
            isOpen={this.state.ownershipDialogVisible}
            onClose={this.onOwnershipDialogToggle}
          >
            <div className={Classes.DIALOG_BODY}>
              {separateOwnership(this.props.payload)}
            </div>
          </Dialog>

          <Dialog
            style={{ width: "85%" }}
            title="Audit Info"
            isOpen={this.state.handlerDialogVisible}
            onClose={this.onHandlerDialogToggle}
          >
            <div className={Classes.DIALOG_BODY}>
              {separateHandlerPublicKey(this.props.payload)}
            </div>
          </Dialog>

          <Dialog
            style={{ width: "85%" }}
            title="Image"
            isOpen={this.state.imageDialogVisible}
            onClose={this.onImageDialogToggle}
          >
            <div className={Classes.DIALOG_BODY}>
              {
                <>
                  {this.props.payload &&
                    JSON.parse(this.props.payload).payload &&
                    JSON.parse(this.props.payload).payload.item_image && (
                      <img
                        src={`data:image/jpg;base64, ${
                          JSON.parse(this.props.payload).payload.item_image
                        }`}
                      />
                    )}
                  <br />
                </>
              }
            </div>
          </Dialog>

          <ButtonGroup vertical={true}>
            {
              <>
                {this.props.payload &&
                  JSON.parse(this.props.payload).payload &&
                  JSON.parse(this.props.payload).payload.item_image && (
                    <Button intent="none" onClick={this.onImageDialogToggle}>
                      Show Image
                    </Button>
                  )}
              </>
            }
            <Button intent="none" onClick={this.onOwnershipDialogToggle}>
              Ownership Info
            </Button>
            <Button intent="danger" onClick={this.onHandlerDialogToggle}>
              Audit Info
            </Button>
            <Divider />
            <Button intent="primary" onClick={this.onContinueDialogToggle}>
              Continue trace from here
            </Button>
            <Button intent="none" onClick={this.onRestartDialogToggle}>
              Start trace from here
            </Button>
          </ButtonGroup>
        </Card>
      </div>
    );
  }
}

export default DetailsPanel;
