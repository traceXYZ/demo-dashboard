import { GraphResult, ITraceStartDialogState } from "./TraceTypes";

export enum TraceRequestActionType {
  TRACE_START = "TRACE_START_REQUEST_SEND",
  TRACE_CONTINUE = "TRACE_CONTINUE_REQUEST_SEND"
}

export enum TraceResponseActionType {
  TRACE_START = "TRACE_START_RESPONSE_RECEIVE",
  TRACE_CONTINUE = "TRACE_CONTINUE_RESPONSE_RECEIVE"
}

export type TraceRequestAction = {
  type: TraceRequestActionType;
  payload: { query: ITraceStartDialogState; token: string };
};

export type TraceResponseAction = {
  type: TraceResponseActionType;
  result?: GraphResult;
  error?: string;
};

export type TraceAction = TraceRequestAction | TraceResponseAction;

export function sendTraceStart(
  query: ITraceStartDialogState,
  token: string
): TraceRequestAction {
  return {
    type: TraceRequestActionType.TRACE_START,
    payload: { query, token }
  };
}

export function sendTraceContinue(
  query: ITraceStartDialogState,
  token: string
): TraceRequestAction {
  return {
    type: TraceRequestActionType.TRACE_CONTINUE,
    payload: { query, token }
  };
}

export function receiveTraceStart(result: GraphResult): TraceResponseAction {
  return {
    type: TraceResponseActionType.TRACE_START,
    error: !result ? "Object not found" : null,
    result
  };
}

export function receiveTraceContinue(result: GraphResult): TraceResponseAction {
  return {
    type: TraceResponseActionType.TRACE_CONTINUE,
    error: !result ? "Object not found" : null,
    result
  };
}
