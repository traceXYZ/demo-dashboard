import { connect } from "react-redux";

import Trace from "./Trace";
import { sendTraceStart, sendTraceContinue } from "./actions";
import { ITraceState } from "./reducer";
import { ITraceStartDialogState } from "./TraceTypes";
import { User } from "../MultiLogin/MultiLoginTypes";

const mapStateToProps = (state: {
  trace: ITraceState;
  logins: { users: Array<User> };
}) => {
  return { ...state.trace, users: state.logins.users };
};

const mapDispatchToProps = dispatch => {
  return {
    onStartTrace: (query: ITraceStartDialogState, token: string) => {
      dispatch(sendTraceStart(query, token));
    },

    onContinueTrace: (query: ITraceStartDialogState, token: string) => {
      dispatch(sendTraceContinue(query, token));
    }
  };
};

const TracePage = connect(
  mapStateToProps,
  mapDispatchToProps
)(Trace);

export default TracePage;
