import { drag as d3Drag } from "d3-drag";
import { forceLink as d3ForceLink } from "d3-force";
import {
  select as d3Select,
  selectAll as d3SelectAll,
  event as d3Event
} from "d3-selection";
import { zoom as d3Zoom } from "d3-zoom";
import { Graph } from "react-d3-graph";

export default class XGraph extends Graph {
  constructor(props) {
    super(props);
  }

  _graphForcesConfig() {
    this.state.simulation.nodes(this.state.d3Nodes).on("tick", this._tick);

    const forceLink = d3ForceLink(this.state.d3Links)
      .id(l => l.id)
      .distance(this.state.config.d3.linkLength)
      .strength(this.state.config.d3.linkStrength);

    this.state.simulation.force("link", forceLink);

    const customNodeDrag = d3Drag()
      .on("start", this._onDragStart)
      .on("drag", this._onDragMove)
      .on("end", this._onDragEnd);

    d3Select(`#${this.state.id}-${"graph-wrapper"}`)
      .selectAll(".node")
      .call(customNodeDrag);

    const svg = d3Select(`#${this.state.id}-graph-wrapper`).select("svg");
    svg
      .append("defs")
      .append("marker")
      .attr("id", "arrowhead")
      .attr("viewBox", "-0 -5 10 10")
      .attr("refX", 25)
      .attr("refY", -2)
      .attr("orient", "auto")
      .attr("markerWidth", 7)
      .attr("markerHeight", 7)
      .attr("xoverflow", "visible")
      .append("svg:path")
      .attr("d", "M 0,-5 L 10,0 L 0,5")
      .attr("fill", "#999")
      .style("stroke", "none");

    d3Select(`#${this.state.id}-${"graph-wrapper"}`)
      .selectAll(".link")
      .attr("marker-end", "url(#arrowhead)");
  }
}
