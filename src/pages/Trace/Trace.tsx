import * as React from "react";
import { Button } from "@blueprintjs/core";

import DetailsPanel from "./DetailsPanel";
import TraceStartDialog from "./TraceStartDialog";
import { ITraceStartDialogState } from "./TraceTypes";
import GraphView from "./GraphView";
import { graphToD3 } from "./graphTransformer";

import { User } from "../MultiLogin/MultiLoginTypes";
import UserSelector from "../../components/UserSelector";

const itemDataForId = (id, data) => {
  const found = data.filter(it => it.id === id);
  return (found && found[0]) || {};
};

interface ITracePageProps {
  onStartTrace: (state: ITraceStartDialogState, token: string) => void;
  onContinueTrace: (state: ITraceStartDialogState, token: string) => void;

  result: any;
  error: string;
  loading: boolean;
}

interface IAugmentedTracePageProps extends ITracePageProps {
  users: Array<User>;
}

interface ITracePageState {
  user: User;
  selectedId: string;
  startDialogVisible: boolean;
}

class TracePage extends React.Component<
  IAugmentedTracePageProps,
  ITracePageState
> {
  constructor(props) {
    super(props);

    this.state = {
      user: props.users && props.users[0],
      selectedId: null,
      startDialogVisible: false
    };
  }

  onUserSelect = user => {
    this.setState({ user });
  };

  onTraceStartClick = (query: ITraceStartDialogState) => {
    this.props.onStartTrace(query, this.state.user.token);
  };

  onTraceMoreClick = (query: ITraceStartDialogState, contd: Boolean) => {
    if (contd) {
      this.props.onContinueTrace(query, this.state.user.token);
    } else {
      this.props.onStartTrace(query, this.state.user.token);
    }
  };

  onStartDialogToggle = () => {
    this.setState({ startDialogVisible: !this.state.startDialogVisible });
  };

  onGraphSelectNode = (id: string) => {
    this.setState({ selectedId: id });
  };

  render() {
    return (
      <>
        <UserSelector
          onUserSelect={this.onUserSelect}
          users={this.props.users}
          selectedUser={this.state.user}
        />

        <Button intent="primary" onClick={this.onStartDialogToggle}>
          Trace an Item
        </Button>

        <div style={{ display: "flex", flexDirection: "row" }}>
          {this.props.result &&
            this.props.result.nodes &&
            this.props.result.nodes.length > 0 && (
              <GraphView
                data={graphToD3(this.props.result)}
                onSelectNode={this.onGraphSelectNode}
              />
            )}

          {this.state.selectedId && (
            <DetailsPanel
              loading={this.props.loading}
              error={this.props.error}
              id={this.state.selectedId}
              key={this.state.selectedId}
              onTraceClick={this.onTraceMoreClick}
              payload={JSON.stringify(
                itemDataForId(
                  this.state.selectedId,
                  this.props.result.storage_items
                ),
                null,
                2
              )}
            />
          )}

          <TraceStartDialog
            onStartDialogToggle={this.onStartDialogToggle}
            startDialogVisible={this.state.startDialogVisible}
            onTraceClick={this.onTraceStartClick}
            loading={this.props.loading}
            error={this.props.error}
            idLocked={false}
          />
        </div>
      </>
    );
  }
}

export default TracePage;
