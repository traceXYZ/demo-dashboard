import * as React from "react";
import { Card, Classes } from "@blueprintjs/core";

import { User } from "../MultiLogin/MultiLoginTypes";
import { OwningItemProxy, HandlingProcessProxy } from "./actions";
import HandlingProcessSelector from "./HandlingProcessSelector";
import OwningItemSelector from "./OwningItemSelector";
import UserSelector from "../../components/UserSelector";

interface IWatchlistPageProps {
  users: Array<User>;

  owningItemProxies: Array<OwningItemProxy>;
  owningItems: Array<any>;

  handlingProcessProxies: Array<HandlingProcessProxy>;
  handlingProcesses: Array<any>;

  onFetchHandlingProcesses: (token: string) => void;
  onFetchOwningItems: (token: string) => void;

  onFetchOwningItemInfo: (id: string, token: string) => void;
  onFetchHandlingProcessInfo: (id: string, token: string) => void;
}

interface IWatchlistPageState {
  user: User;

  selectedOwningItemProxy: OwningItemProxy;
  selectedHandlingProcessProxy: HandlingProcessProxy;
}

class WatchlistPage extends React.Component<
  IWatchlistPageProps,
  IWatchlistPageState
> {
  constructor(props) {
    super(props);

    this.state = {
      user: this.props.users && this.props.users[0],
      selectedOwningItemProxy: null,
      selectedHandlingProcessProxy: null
    };
  }

  onUserSelect = user => {
    this.setState({ user });
    this.props.onFetchOwningItems(user.token);
    this.props.onFetchHandlingProcesses(user.token);
    this.setState({ selectedOwningItemProxy: null });
  };

  onOwningItemSelect = (item: OwningItemProxy) => {
    this.setState({ selectedOwningItemProxy: item });
    this.props.onFetchOwningItemInfo(item.id, this.state.user.token);
  };

  onHandlingProcessSelect = (process: HandlingProcessProxy) => {
    this.setState({ selectedHandlingProcessProxy: process });
    this.props.onFetchHandlingProcessInfo(process.id, this.state.user.token);
  };

  render() {
    return (
      <>
        <UserSelector
          users={this.props.users}
          onUserSelect={this.onUserSelect}
          selectedUser={this.state.user}
        />

        <OwningItemSelector
          onOwningItemSelect={this.onOwningItemSelect}
          owningItems={this.props.owningItemProxies}
          selectedOwningItem={this.state.selectedOwningItemProxy}
        />
        {this.state.selectedOwningItemProxy && (
          <Card
            className={
              this.props.owningItems.filter(
                item => item.id === this.state.selectedOwningItemProxy.id
              ).length < 1
                ? Classes.SKELETON
                : ""
            }
          >
            <h3>{this.state.selectedOwningItemProxy.type}</h3>
            <p>ID: {this.state.selectedOwningItemProxy.id}</p>
            <pre>
              {
                (
                  this.props.owningItems.filter(
                    item => item.id === this.state.selectedOwningItemProxy.id
                  )[0] || { payload: "loading..." }
                ).payload
              }
            </pre>
          </Card>
        )}

        <HandlingProcessSelector
          onHandlingProcessSelect={this.onHandlingProcessSelect}
          handlingProcesses={this.props.handlingProcessProxies}
          selectedHandlingProcess={this.state.selectedHandlingProcessProxy}
        />
        {this.state.selectedHandlingProcessProxy && (
          <Card
            className={
              this.props.handlingProcesses.filter(
                item => item.id === this.state.selectedHandlingProcessProxy.id
              ).length < 1
                ? Classes.SKELETON
                : ""
            }
          >
            <h3>{this.state.selectedHandlingProcessProxy.process}</h3>
            <p>ID: {this.state.selectedHandlingProcessProxy.id}</p>
            <pre>
              {
                (
                  this.props.handlingProcesses.filter(
                    item =>
                      item.id === this.state.selectedHandlingProcessProxy.id
                  )[0] || { process: "loading..." }
                ).payload
              }
            </pre>
          </Card>
        )}
      </>
    );
  }
}

export default WatchlistPage;
