import {
  PutEffect,
  ForkEffect,
  put,
  call,
  takeEvery
} from "redux-saga/effects";
import {
  WatchlistActionType,
  receiveFetchOwningItems,
  receiveFetchOwningItemInfo,
  receiveFetchHandlingProcesses,
  receiveFetchHandlingProcessInfo
} from "./actions";
import { getOwnedItems, query, getHandledItems } from "../../services/api";

export function* watchFetchOwningItems(): Iterator<ForkEffect> {
  yield takeEvery(WatchlistActionType.GET_OWNING, fetchOwningItems);
}

const cleanJson = json => {
  return JSON.stringify(JSON.parse(json), null, 2);
};

function* fetchOwningItems(action) {
  try {
    const json = yield call(getOwnedItems, action.token);
    const obj = JSON.parse(json);
    if (obj.error) {
      yield put(receiveFetchOwningItems(null, obj.error));
    } else {
      const arrOwnedItems = Object.keys(obj).map(k => ({ id: k, ...obj[k] }));
      yield put(receiveFetchOwningItems(arrOwnedItems, null));
    }
  } catch (error) {
    yield put(receiveFetchOwningItems(null, error.message));
  }
}

export function* watchFetchOwningItemInfo(): Iterator<ForkEffect> {
  yield takeEvery(WatchlistActionType.GET_OWNING_INFO, fetchOwningItemInfo);
}

function* fetchOwningItemInfo(action) {
  try {
    const payload = {
      chaincodeId: "storage_cc",
      fcn: "retrieve_direct",
      args: [action.id],
      chainId: "cheeseproduction"
    };
    const json = yield call(query, JSON.stringify(payload), action.token);
    const obj = JSON.parse(json);
    if (obj.error) {
      yield put(receiveFetchOwningItemInfo(null, obj.error));
    } else {
      yield put(
        receiveFetchOwningItemInfo(
          {
            id: obj.storage_item.id,
            payload: cleanJson(atob(obj.storage_item.payload))
          },
          null
        )
      );
    }
  } catch (error) {
    yield put(receiveFetchOwningItemInfo(null, error.message));
  }
}

export function* watchFetchHandlingProcesses(): Iterator<ForkEffect> {
  yield takeEvery(WatchlistActionType.GET_OWNING, fetchHandlingProcesses);
}

function* fetchHandlingProcesses(action) {
  try {
    const json = yield call(getHandledItems, action.token);
    const obj = JSON.parse(json);
    if (obj.error) {
      yield put(receiveFetchHandlingProcesses(null, obj.error));
    } else {
      const arrOwnedItems = Object.keys(obj).map(k => ({ id: k, ...obj[k] }));
      yield put(receiveFetchHandlingProcesses(arrOwnedItems, null));
    }
  } catch (error) {
    yield put(receiveFetchHandlingProcesses(null, error.message));
  }
}

export function* watchFetchHandlingProcessesInfo(): Iterator<ForkEffect> {
  yield takeEvery(
    WatchlistActionType.GET_HANDLING_INFO,
    fetchHandlingProcessesInfo
  );
}

function* fetchHandlingProcessesInfo(action) {
  try {
    const payload = {
      chaincodeId: "storage_cc",
      fcn: "retrieve_direct",
      args: [action.id],
      chainId: "cheeseproduction"
    };
    const json = yield call(query, JSON.stringify(payload), action.token);
    const obj = JSON.parse(json);
    if (obj.error) {
      yield put(receiveFetchHandlingProcessInfo(null, obj.error));
    } else {
      yield put(
        receiveFetchHandlingProcessInfo(
          {
            id: obj.storage_item.id,
            payload: cleanJson(atob(obj.storage_item.payload))
          },
          null
        )
      );
    }
  } catch (error) {
    yield put(receiveFetchHandlingProcessInfo(null, error.message));
  }
}
