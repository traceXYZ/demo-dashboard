import { connect } from "react-redux";
import { User } from "../MultiLogin/MultiLoginTypes";

import Watchlist from "./Watchlist";
import {
  sendFetchOwningItems,
  sendFetchHandlingProcesses,
  sendFetchOwningItemInfo,
  sendFetchHandlingProcessInfo
} from "./actions";

const mapStateToProps = (state: {
  logins: { users: Array<User> };
  watchlist: any;
}) => {
  return {
    users: state.logins.users,
    ...state.watchlist
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchOwningItems: (token: string) => {
      dispatch(sendFetchOwningItems(token));
    },

    onFetchHandlingProcesses: (token: string) => {
      dispatch(sendFetchHandlingProcesses(token));
    },

    onFetchOwningItemInfo: (id: string, token: string) => {
      dispatch(sendFetchOwningItemInfo(id, token));
    },

    onFetchHandlingProcessInfo: (id: string, token: string) => {
      dispatch(sendFetchHandlingProcessInfo(id, token));
    }
  };
};

const WatchlistPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(Watchlist);

export default WatchlistPage;
