import * as React from "react";
import { Select, ItemRenderer } from "@blueprintjs/select";
import { Button, MenuItem } from "@blueprintjs/core";

import { OwningItemProxy } from "./actions";

const OwningItemSelect = Select.ofType<OwningItemProxy>();

const renderOwningItem: ItemRenderer<OwningItemProxy> = (
  owningItem,
  { handleClick, modifiers }
) => {
  return (
    <MenuItem
      active={modifiers.active}
      key={owningItem.id}
      onClick={handleClick}
      text={owningItem.type}
    />
  );
};

interface IOwningItemSelectorProps {
  onOwningItemSelect: (item: OwningItemProxy) => void;

  owningItems: Array<OwningItemProxy>;
  selectedOwningItem: OwningItemProxy;
}

class OwningItemSelector extends React.Component<IOwningItemSelectorProps> {
  constructor(props) {
    super(props);
  }

  onOwningItemSelect = item => {
    this.props.onOwningItemSelect(item);
  };

  render() {
    return (
      <div>
        <OwningItemSelect
          onItemSelect={this.onOwningItemSelect}
          itemRenderer={renderOwningItem}
          itemPredicate={(q, x, i) => x.type.includes(q)}
          items={this.props.owningItems}
        >
          <Button
            rightIcon="caret-down"
            icon="box"
            intent="none"
            text={
              (this.props.selectedOwningItem &&
                this.props.selectedOwningItem.type) ||
              "Select items you own"
            }
          />
        </OwningItemSelect>
      </div>
    );
  }
}

export default OwningItemSelector;
