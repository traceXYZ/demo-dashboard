import { WatchlistActionType } from "./actions";

const defaultState = {
  owningItemProxies: [],
  owningItems: [],
  handlingProcessProxies: [],
  handlingProcesses: []
};

export function watchlist(state = defaultState, action) {
  switch (action.type) {
    case WatchlistActionType.GET_OWNING: // Saga
      return state;
    case WatchlistActionType.GET_HANDLING: // Saga
      return state;
    case WatchlistActionType.GET_OWNING_INFO: // Saga
      return state;
    case WatchlistActionType.GET_HANDLING_INFO: // Saga
      return state;

    case WatchlistActionType.GOT_OWNING:
      if (action.error) {
        return state;
      }
      return {
        ...state,
        owningItemProxies: action.result || state.owningItemProxies
      };

    case WatchlistActionType.GOT_HANDLING:
      if (action.error) {
        return state;
      }
      return {
        ...state,
        handlingProcessProxies: action.result || state.handlingProcessProxies
      };

    case WatchlistActionType.GOT_OWNING_INFO:
      if (action.error) {
        return state;
      }
      return {
        ...state,
        owningItems: [
          ...state.owningItems.filter(id => id !== action.result.id),
          action.result
        ]
      };

    case WatchlistActionType.GOT_HANDLING_INFO:
      if (action.error) {
        return state;
      }
      return {
        ...state,
        handlingProcesses: [
          ...state.handlingProcesses.filter(id => id !== action.result.id),
          action.result
        ]
      };

    default:
      return state;
  }
}
