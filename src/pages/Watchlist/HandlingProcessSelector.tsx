import * as React from "react";
import { Select, ItemRenderer } from "@blueprintjs/select";
import { Button, MenuItem } from "@blueprintjs/core";

import { HandlingProcessProxy } from "./actions";

const HandlingProcessSelect = Select.ofType<HandlingProcessProxy>();

const renderHandlingProcess: ItemRenderer<HandlingProcessProxy> = (
  handlingProcess,
  { handleClick, modifiers }
) => {
  return (
    <MenuItem
      active={modifiers.active}
      key={handlingProcess.id}
      onClick={handleClick}
      text={handlingProcess.process}
    />
  );
};

interface IHandlingProcessSelectorProps {
  onHandlingProcessSelect: (handle: HandlingProcessProxy) => void;

  handlingProcesses: Array<HandlingProcessProxy>;
  selectedHandlingProcess: HandlingProcessProxy;
}

class HandlingProcessSelector extends React.Component<
  IHandlingProcessSelectorProps
> {
  constructor(props) {
    super(props);
  }

  onHandlingProcessSelect = handle => {
    this.props.onHandlingProcessSelect(handle);
  };

  render() {
    return (
      <div>
        <HandlingProcessSelect
          onItemSelect={this.onHandlingProcessSelect}
          itemRenderer={renderHandlingProcess}
          itemPredicate={(q, x, i) => x.process.includes(q)}
          items={this.props.handlingProcesses}
        >
          <Button
            rightIcon="caret-down"
            icon="hand"
            intent="none"
            text={
              (this.props.selectedHandlingProcess &&
                this.props.selectedHandlingProcess.process) ||
              "Select processes you handle"
            }
          />
        </HandlingProcessSelect>
      </div>
    );
  }
}

export default HandlingProcessSelector;
