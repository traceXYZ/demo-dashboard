export type OwningItemProxy = { id: string; type: string };

export type HandlingProcessProxy = { id: string; process: string };

export type OwningItem = { id: string; payload: string };

export type HandlingProcess = { id: string; payload: string };

export enum WatchlistActionType {
  GET_OWNING = "OWNING_FETCH_REQUEST_SEND",
  GOT_OWNING = "OWNING_FETCH_RESPONSE_RECEIVE",

  GET_OWNING_INFO = "OWNING_INFO_REQUEST_SEND",
  GOT_OWNING_INFO = "OWNING_INFO_RESPONSE_RECEIVE",

  GET_HANDLING = "HANDKING_FETCH_REQUEST_SEND",
  GOT_HANDLING = "HANDLING_FETCH_RESPONSE_RECEIVE",

  GET_HANDLING_INFO = "HANDLING_INFO_REQUEST_SEND",
  GOT_HANDLING_INFO = "HANDLING_INFO_RESPONSE_RECEIVE"
}

export function sendFetchOwningItems(token: string) {
  return { type: WatchlistActionType.GET_OWNING, token };
}

export function receiveFetchOwningItems(
  result: Array<OwningItemProxy>,
  error: string
) {
  return { type: WatchlistActionType.GOT_OWNING, result, error };
}

export function sendFetchOwningItemInfo(id: string, token: string) {
  return { type: WatchlistActionType.GET_OWNING_INFO, id, token };
}

export function receiveFetchOwningItemInfo(result: OwningItem, error: string) {
  return { type: WatchlistActionType.GOT_OWNING_INFO, result, error };
}

export function sendFetchHandlingProcesses(token: string) {
  return { type: WatchlistActionType.GET_HANDLING, token };
}

export function receiveFetchHandlingProcesses(
  result: Array<HandlingProcessProxy>,
  error: string
) {
  return { type: WatchlistActionType.GOT_HANDLING, result, error };
}

export function sendFetchHandlingProcessInfo(id: string, token: string) {
  return { type: WatchlistActionType.GET_HANDLING_INFO, id, token };
}

export function receiveFetchHandlingProcessInfo(
  result: HandlingProcess,
  error: string
) {
  return { type: WatchlistActionType.GOT_HANDLING_INFO, result, error };
}
