import { Credential, User } from "./MultiLoginTypes";

export enum LoginRequestActionType {
  LOGIN = "USER_LOGIN_REQUEST_SEND"
}

export enum LoginResponseActionType {
  LOGIN = "USER_LOGIN_RESPONSE_RECEIVE"
}

export enum LogoutActionType {
  LOGOUT = "USER_LOGOUT"
}

export type LoginRequestAction = {
  type: LoginRequestActionType;
  payload: Credential;
};

export type LoginResponseAction = {
  type: LoginResponseActionType;
  result?: User;
  error?: string;
};

export type LogoutAction = {
  type: LogoutActionType;
  name: string;
};

export type MultiLoginAction =
  | LoginRequestAction
  | LoginResponseAction
  | LogoutAction;

export function sendLoginUser(payload: Credential): LoginRequestAction {
  return {
    type: LoginRequestActionType.LOGIN,
    payload
  };
}

export function receiveLoginUser(result: User): LoginResponseAction {
  return {
    type: LoginResponseActionType.LOGIN,
    result
  };
}

export function logoutUser(name: string): LogoutAction {
  return {
    type: LogoutActionType.LOGOUT,
    name
  };
}
