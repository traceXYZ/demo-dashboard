import {
  LoginRequestActionType,
  LoginResponseActionType,
  MultiLoginAction,
  LogoutActionType
} from "./actions";
import { User } from "./MultiLoginTypes";

export interface ILoginsState {
  loading: boolean;
  users: Array<User>;
  error: string;
}

const defaultState: ILoginsState = {
  loading: false,
  users: [],
  error: ""
};

export function logins(
  state = defaultState,
  action: MultiLoginAction
): ILoginsState {
  switch (action.type) {
    case LoginRequestActionType.LOGIN:
      return { loading: true, users: state.users, error: "" };

    case LoginResponseActionType.LOGIN:
      if (action.error) {
        return { loading: false, error: action.error, users: state.users };
      }
      if (!action.result) {
        return {
          loading: false,
          error: "Username or password is incorrect.",
          users: state.users
        };
      }
      return {
        loading: false,
        users: [
          ...state.users.filter(user => user.name !== action.result.name),
          action.result
        ],
        error: ""
      };

    case LogoutActionType.LOGOUT:
      return {
        error: "",
        loading: false,
        users: state.users.filter(user => user.name !== action.name)
      };

    default:
      return state || defaultState;
  }
}
