import { connect } from "react-redux";

import MultiLogin from "./MultiLogin";

import { sendLoginUser, logoutUser } from "./actions";
import { ILoginsState } from "./reducer";
import { Credential } from "./MultiLoginTypes";

const mapStateToProps = (state: { logins: ILoginsState }) => {
  return state.logins;
};

const mapDispatchToProps = dispatch => {
  return {
    onLoginClick: (cred: Credential) => {
      dispatch(sendLoginUser(cred));
    },

    onLogoutClick: (name: string) => {
      dispatch(logoutUser(name));
    }
  };
};

const MultiLoginPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(MultiLogin);

export default MultiLoginPage;
