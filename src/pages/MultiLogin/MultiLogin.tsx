import * as React from "react";
import {
  FormGroup,
  InputGroup,
  ProgressBar,
  Button,
  Divider,
  Card,
  Toaster,
  Position,
  Tag,
  H5
} from "@blueprintjs/core";

import { Credential, User } from "./MultiLoginTypes";

interface IMultiLoginPageProps {
  onLoginClick: (cred: Credential) => void;
  onLogoutClick: (name: string) => void;

  loading: boolean;
  error: string;
  users: Array<User>;
}

interface IMultiLoginPageState {
  username: string;
  password: string;
}

const AppToaster = Toaster.create({
  className: "login-toaster",
  position: Position.TOP
});

class MultiLoginPage extends React.Component<
  IMultiLoginPageProps,
  IMultiLoginPageState
> {
  constructor(props) {
    super(props);

    this.state = { username: "", password: "" };
  }

  componentWillReceiveProps = (
    nextProps: IMultiLoginPageProps,
    nextContext
  ) => {
    if (nextProps.error) {
      AppToaster.show({
        intent: "danger",
        icon: "error",
        message: nextProps.error
      });
    }
  };

  onUsernameChange = event => {
    this.setState({ username: event.target.value });
  };

  onPasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  onLoginClick = () => {
    this.props.onLoginClick(this.state);
  };

  onLogoutClick = name => {
    this.props.onLogoutClick(name);
  };

  render() {
    return (
      <>
        <Card>
          <H5>Logged In: </H5>
          {this.props.users.map(user => (
            <Tag
              large
              key={user.name}
              onRemove={() => this.onLogoutClick(user.name)}
            >
              {user.name}
            </Tag>
          ))}
        </Card>
        <Divider />
        <FormGroup
          helperText="Uniquely identify the user"
          label="Username"
          labelFor="create-user-username"
        >
          <InputGroup
            id="create-user-username"
            placeholder={"johndoe"}
            onChange={this.onUsernameChange}
            value={this.state.username}
          />
        </FormGroup>
        <FormGroup
          helperText="Enter something that you remember well, but others don't"
          label="Password"
          labelFor="create-user-password"
        >
          <InputGroup
            type="password"
            id="create-user-password"
            placeholder={"correct horse battery staple"}
            onChange={this.onPasswordChange}
            value={this.state.password}
          />
        </FormGroup>

        <ProgressBar
          animate={this.props.loading}
          stripes={this.props.loading}
          intent="primary"
        />

        <Button intent="success" onClick={this.onLoginClick}>
          Login
        </Button>
      </>
    );
  }
}

export default MultiLoginPage;
