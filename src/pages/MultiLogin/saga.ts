import {
  put,
  takeEvery,
  ForkEffect,
  PutEffect,
  call
} from "redux-saga/effects";
import {
  LoginRequestActionType,
  receiveLoginUser,
  LoginResponseAction,
  LoginRequestAction
} from "./actions";
import { loginUser } from "../../services/api";
import { User } from "./MultiLoginTypes";

export function* watchLoginUserAsync(): Iterator<ForkEffect> {
  yield takeEvery(LoginRequestActionType.LOGIN, loginUserAsync);
}

function* loginUserAsync(
  action: LoginRequestAction
): AsyncIterableIterator<PutEffect<LoginResponseAction>> {
  let json = yield call(loginUser, JSON.stringify(action.payload));

  try {
    const user = JSON.parse(json) as User;
    yield put(receiveLoginUser({ ...user, name: action.payload.username }));
  } catch (error) {
    yield put(receiveLoginUser(null));
  }
}
