import {
  ProduceAction,
  FetchSpecRequestActionType,
  FetchSpecResponseActionType,
  CreateItemRequestActionType,
  CreateItemResponseActionType
} from "./actions";
import { IProduceItemState } from "./CreateTypes";

const defaultState: IProduceItemState = {
  specifications: [],

  loading: false,
  result: "",
  error: ""
};

export function produce(
  state = defaultState,
  action: ProduceAction
): IProduceItemState {
  switch (action.type) {
    case FetchSpecRequestActionType.FETCH: // Caught by Saga
      return state;

    case FetchSpecResponseActionType.FETCH:
      if (action.payload) {
        return { ...state, specifications: action.payload };
      }

    case CreateItemRequestActionType.CREATE: // Caught by Saga
      return { ...state, loading: true };

    case CreateItemResponseActionType.CREATE:
      if (action.error) {
        return { ...state, result: "", error: action.error, loading: false };
      }
      return { ...state, error: "", result: action.result, loading: false };
    default:
      return state;
  }
}
