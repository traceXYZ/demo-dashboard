export interface Specification {
  name: string;
  id: string;
}

export interface IProduceItemState {
  specifications: Array<Specification>;

  loading: boolean;
  result: string;
  error: string;
}
