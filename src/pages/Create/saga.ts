import {
  put,
  takeEvery,
  ForkEffect,
  PutEffect,
  call
} from "redux-saga/effects";
import {
  FetchSpecRequestActionType,
  CreateItemRequestActionType,
  FetchSpecResponseActionType,
  FetchSpecRequestAction,
  FetchSpecResponseAction,
  receiveFetchSpecifications,
  CreateItemRequestAction,
  CreateItemResponseAction,
  receiveCreateItem
} from "./actions";
import { fetchSpecifications, createItem } from "../../services/api";
import { Specification } from "./CreateTypes";

export function* watchFetchSpecAsync(): Iterator<ForkEffect> {
  yield takeEvery(FetchSpecRequestActionType.FETCH, fetchSpecAsync);
}

function* fetchSpecAsync(
  action: FetchSpecRequestAction
): AsyncIterableIterator<PutEffect<FetchSpecResponseAction>> {
  let json = yield call(fetchSpecifications, action.token);

  try {
    const specs = JSON.parse(json).specifications.map(s => ({
      id: s.spec_id,
      name: s.spec_name
    })) as Array<Specification>;
    yield put(receiveFetchSpecifications(specs));
  } catch (error) {
    yield put(receiveFetchSpecifications(null));
  }
}

export function* watchCreateItemAsync(): Iterator<ForkEffect> {
  yield takeEvery(CreateItemRequestActionType.CREATE, createItemAsync);
}

function* createItemAsync(
  action: CreateItemRequestAction
): AsyncIterableIterator<PutEffect<CreateItemResponseAction>> {
  try {
    let result = yield call(
      createItem,
      JSON.stringify(action.payload),
      action.token
    );

    const parsed = JSON.parse(result);
    if (parsed.error) {
      yield put(receiveCreateItem(null, parsed.error));
    } else {
      yield put(receiveCreateItem(result, null));
    }
  } catch (error) {
    yield put(receiveCreateItem(null, error.message));
  }
}
