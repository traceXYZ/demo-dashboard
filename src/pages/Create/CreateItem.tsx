import * as React from "react";
import {
  FormGroup,
  InputGroup,
  Button,
  Divider,
  H5,
  ButtonGroup,
  ProgressBar,
  Callout
} from "@blueprintjs/core";

import UserSelector from "../../components/UserSelector";
import { User } from "../MultiLogin/MultiLoginTypes";
import { Item } from "./Item";
import { ProductionProcess } from "./ProductionProcess";
import { Row } from "../../components/Layout/Flex";
import examples from "./examples";
import SpecSelector from "../../components/SpecSelector";
import { Specification } from "./CreateTypes";

interface ICreateItemPageProps {
  onCreateItem: (
    chaincodeId: string,
    fcn: string,
    specification: string,
    item: Object,
    productionProcess: Object,
    token: string
  ) => void;
  loadSpecifications: (token: string) => void;

  users: Array<User>;
  specifications: Array<Specification>;

  loading: boolean;
  result?: string;
  error?: string;
}

interface ICreateItemPageState {
  user: User;
  spec: Specification;

  exampleName: string;
  item: string;
  productionProcess: string;
  chaincodeId: string;
  fcn: string;
}

class CreateItemPage extends React.Component<
  ICreateItemPageProps,
  ICreateItemPageState
> {
  constructor(props) {
    super(props);

    this.state = {
      user: this.props.users && this.props.users[0],
      item: "",
      productionProcess: "",
      exampleName: "",
      chaincodeId: "",
      fcn: "",
      spec: this.props.specifications && this.props.specifications[0]
    };
  }

  componentDidMount() {
    if (!this.props.specifications || this.props.specifications.length < 1) {
      if (this.state.user) {
        this.props.loadSpecifications(this.state.user.token);
      }
    }
  }

  onUserSelect = (user: User) => {
    this.setState({ user });
  };

  onChaincodeIdChange = event => {
    this.setState({ chaincodeId: event.target.value });
  };

  onFcnChange = event => {
    this.setState({ fcn: event.target.value });
  };

  onSpecSelect = spec => {
    this.setState({ spec });
  };

  onItemChange = (json: string) => {
    this.setState({ item: json });
  };

  onProductionProcessChange = (json: string) => {
    this.setState({ productionProcess: json });
  };

  onExampleSelect = (name: string) => {
    this.setState({
      ...examples[name],
      exampleName: name
    });
  };

  onCreateItem = () => {
    const {
      chaincodeId,
      fcn,
      spec,
      item,
      productionProcess,
      user
    } = this.state;
    this.props.onCreateItem(
      chaincodeId,
      fcn,
      (spec && spec.id) || "",
      JSON.parse(item),
      JSON.parse(productionProcess),
      user.token
    );
  };

  render() {
    return (
      <div>
        <UserSelector
          users={this.props.users}
          onUserSelect={this.onUserSelect}
          selectedUser={this.state.user}
        />

        <Row>
          <FormGroup
            style={{ flexGrow: 1 }}
            helperText="Enter the name of the chaincode assigned to you"
            label="Chaincode Id"
            labelFor="chaincode-id-input"
            labelInfo="(required)"
          >
            <InputGroup
              id="chaincode-id-input"
              placeholder="milkproducer_cc"
              value={this.state.chaincodeId}
              onChange={this.onChaincodeIdChange}
            />
          </FormGroup>

          <FormGroup
            style={{ flexGrow: 1 }}
            helperText="Enter the name of the function assigned to you"
            label="Function"
            labelFor="chaincode-function-input"
            labelInfo="(required)"
          >
            <InputGroup
              id="chaincode-function-input"
              placeholder="produce_milk"
              value={this.state.fcn}
              onChange={this.onFcnChange}
            />
          </FormGroup>

          <FormGroup
            style={{ flexGrow: 1 }}
            helperText="Enter name of the specification you adhere to"
            label="Specification"
            labelFor="chaincode-spec-input"
            labelInfo="(optional)"
          >
            <ButtonGroup>
              <SpecSelector
                onSpecSelect={this.onSpecSelect}
                selectedSpec={this.state.spec}
                specs={this.props.specifications}
              />
              <Button
                icon="refresh"
                intent="warning"
                onClick={() =>
                  this.props.loadSpecifications(this.state.user.token)
                }
              />
            </ButtonGroup>
          </FormGroup>
        </Row>

        <H5>Example Products</H5>
        <ButtonGroup>
          {Object.keys(examples).map(k => (
            <Button
              key={k}
              onClick={() => this.onExampleSelect(k)}
              intent="none"
            >
              {k}
            </Button>
          ))}
        </ButtonGroup>
        <Row>
          <Item
            key={`item-${this.state.exampleName}`}
            json={this.state.item}
            onJsonChange={this.onItemChange}
          />
          <ProductionProcess
            key={`pp-${this.state.exampleName}`}
            json={this.state.productionProcess}
            onJsonChange={this.onProductionProcessChange}
          />
        </Row>
        <Divider />
        <ProgressBar
          animate={this.props.loading}
          stripes={this.props.loading}
        />
        <Button intent="primary" onClick={this.onCreateItem}>
          Send
        </Button>

        {this.props.error && (
          <Callout intent="danger">{this.props.error}</Callout>
        )}

        {this.props.result && (
          <Callout intent="success">
            {JSON.parse(this.props.result).storage_network_response.message}
          </Callout>
        )}
      </div>
    );
  }
}

export default CreateItemPage;
