const examples: {
  [x: string]: {
    item: string;
    productionProcess: string;
    fcn: string;
    chaincodeId: string;
  };
} = {
  Milk: {
    fcn: "produce_milk",
    chaincodeId: "milkproducer_cc",
    item: `{
    "itemType": "milk",
    "qty": 260,
    "item_image": "IMG"
}`,
    productionProcess: `{
      "processType": "Milk Production",
      "production_units": "L",
      "farmid": "FARM001",
      "location": "LOCAT1",
      "machine_id": "MACH001"
}`
  }
  //   Salt: {
  //     fcn: "produce_salt",
  //     chaincodeId: "saltproducer_cc",
  //     item: `{
  //     "itemType": "salt",
  //     "qty": 100,
  //     "item_image": "IMG"
  // }`,
  //     productionProcess: `{
  //       "processType": "Salt Production",
  //       "production_units": "KG",
  //       "lakeid": "LK001",
  //       "location": "LOCAT2",
  //       "machine_id": "MACH002"
  // }`
  //   }
};

export default examples;
