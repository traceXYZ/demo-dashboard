import * as React from "react";
import JsonEditor from "../../components/JsonEditor";
import { H5 } from "@blueprintjs/core";
import { Column } from "../../components/Layout/Flex";

interface IProductionProcessProps {
  json: string;
  onJsonChange: (json: string) => void;
}

interface IProductionProcessState {}

export class ProductionProcess extends React.Component<
  IProductionProcessProps,
  IProductionProcessState
> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Column>
        <H5>Production Process Details</H5>
        <JsonEditor
          height="200px"
          json={this.props.json}
          onChange={this.props.onJsonChange}
        />
      </Column>
    );
  }
}
