import { Specification } from "./CreateTypes";

export enum FetchSpecRequestActionType {
  FETCH = "SPEC_FETCH_REQUEST_SEND"
}

export enum FetchSpecResponseActionType {
  FETCH = "SPEC_FETCH_RESPONSE_RECEIVE"
}

export enum CreateItemRequestActionType {
  CREATE = "CREATE_ITEM_REQUEST_SEND"
}

export enum CreateItemResponseActionType {
  CREATE = "CREATE_ITEM_RESPONSE_RECEIVE"
}

export type FetchSpecRequestAction = {
  type: FetchSpecRequestActionType;
  token: string;
};

export type FetchSpecResponseAction = {
  type: FetchSpecResponseActionType;
  payload: Array<Specification>;
};

export type FetchSpecAction = FetchSpecRequestAction | FetchSpecResponseAction;

export type CreateItemRequestAction = {
  type: CreateItemRequestActionType;
  payload: {
    chaincodeId: string;
    fcn: string;
    specification: string;
    item: Object;
    productionProcess: Object;
  };
  token: string;
};

export type CreateItemResponseAction = {
  type: CreateItemResponseActionType;
  result?: string;
  error?: string;
};

export type CreateItemAction =
  | CreateItemRequestAction
  | CreateItemResponseAction;

export type ProduceAction = FetchSpecAction | CreateItemAction;

export function sendFetchSpecifications(token: string): FetchSpecRequestAction {
  return { type: FetchSpecRequestActionType.FETCH, token };
}

export function receiveFetchSpecifications(
  result: Array<Specification>
): FetchSpecResponseAction {
  return {
    type: FetchSpecResponseActionType.FETCH,
    payload: result
  };
}

export function sendCreateItem(
  chaincodeId: string,
  fcn: string,
  specification: string,
  item: Object,
  productionProcess: Object,
  token: string
): CreateItemRequestAction {
  return {
    type: CreateItemRequestActionType.CREATE,
    payload: { chaincodeId, fcn, item, productionProcess, specification },
    token
  };
}

export function receiveCreateItem(
  result: string,
  error: string
): CreateItemResponseAction {
  return { type: CreateItemResponseActionType.CREATE, result, error };
}
