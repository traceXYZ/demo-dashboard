import * as React from "react";
import JsonEditor from "../../components/JsonEditor";
import { H5 } from "@blueprintjs/core";
import { Column } from "../../components/Layout/Flex";

interface IItemProps {
  json: string;
  onJsonChange: (json: string) => void;
}

interface IItemState {}

export class Item extends React.Component<IItemProps, IItemState> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Column>
        <H5>Item details</H5>
        <JsonEditor
          height="200px"
          json={this.props.json}
          onChange={this.props.onJsonChange}
        />
      </Column>
    );
  }
}
