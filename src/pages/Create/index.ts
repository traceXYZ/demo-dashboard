import { connect } from "react-redux";

import CreateItem from "./CreateItem";
import { User } from "../MultiLogin/MultiLoginTypes";
import { sendFetchSpecifications, sendCreateItem } from "./actions";
import { IProduceItemState } from "./CreateTypes";

const mapStateToProps = (state: {
  logins: { users: Array<User> };
  produce: IProduceItemState;
}) => {
  return {
    users: state.logins.users,
    ...state.produce
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadSpecifications: (token: string) => {
      dispatch(sendFetchSpecifications(token));
    },
    onCreateItem: (
      chaincodeId: string,
      fcn: string,
      specification: string,
      item: Object,
      productionProcess: Object,
      token: string
    ) => {
      dispatch(
        sendCreateItem(
          chaincodeId,
          fcn,
          specification,
          item,
          productionProcess,
          token
        )
      );
    }
  };
};

const CreateItemPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateItem);

export default CreateItemPage;
