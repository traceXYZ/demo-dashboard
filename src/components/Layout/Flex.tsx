import * as React from "react";

export function Row({ children, ...rest }) {
  return (
    <div style={{ display: "flex", flexDirection: "row" }}>{children}</div>
  );
}

export function Column({ children, ...rest }) {
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>{children}</div>
  );
}
