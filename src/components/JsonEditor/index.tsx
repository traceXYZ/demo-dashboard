import * as React from "react";
import AceEditor from "react-ace";

import "brace/mode/json";
import "brace/theme/github";

interface IJsonEditorProps {
  height?: string;
  json?: string;
  onChange?: (value: string) => void;
}

interface IJsonEditorState {
  json: string;
}

class JsonEditor extends React.Component<IJsonEditorProps, IJsonEditorState> {
  constructor(props: IJsonEditorProps) {
    super(props);
    this.state = { json: props.json || "" };
  }

  onChange = (value: string, event?: any) => {
    this.setState({ json: value });
    this.props.onChange(value);
  };

  render() {
    return (
      <AceEditor
        style={{ height: this.props.height || "500px" }}
        mode="json"
        theme="github"
        name="jsonEditor"
        fontSize={14}
        showPrintMargin={true}
        showGutter={true}
        highlightActiveLine={true}
        value={this.state.json}
        onChange={this.onChange}
        setOptions={{
          enableBasicAutocompletion: true,
          enableLiveAutocompletion: true,
          enableSnippets: false,
          showLineNumbers: true,
          tabSize: 2
        }}
      />
    );
  }
}

export default JsonEditor;
