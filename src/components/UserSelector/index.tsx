import * as React from "react";
import { Select, ItemRenderer } from "@blueprintjs/select";

import { User } from "../../pages/MultiLogin/MultiLoginTypes";
import { Button, MenuItem } from "@blueprintjs/core";

const UserSelect = Select.ofType<User>();

const renderUser: ItemRenderer<User> = (user, { handleClick, modifiers }) => {
  // if (!modifiers.active) {
  //   return null;
  // }
  return (
    <MenuItem
      active={modifiers.active}
      key={user.name}
      onClick={handleClick}
      text={user.name}
    />
  );
};

interface IUserSelectorProps {
  onUserSelect: (user: User) => void;

  users: Array<User>;
  selectedUser: User;
}

class UserSelector extends React.Component<IUserSelectorProps> {
  constructor(props) {
    super(props);
  }

  onUserSelect = user => {
    this.props.onUserSelect(user);
  };

  render() {
    return (
      <div>
        <UserSelect
          onItemSelect={this.onUserSelect}
          itemRenderer={renderUser}
          itemPredicate={(q, x, i) => x.name.includes(q)}
          items={this.props.users}
        >
          <Button
            rightIcon="caret-down"
            icon="person"
            intent="none"
            text={
              (this.props.selectedUser && this.props.selectedUser.name) ||
              "Select user"
            }
          />
        </UserSelect>
      </div>
    );
  }
}

export default UserSelector;
