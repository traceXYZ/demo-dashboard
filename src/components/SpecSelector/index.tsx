import * as React from "react";
import { Select, ItemRenderer } from "@blueprintjs/select";
import { Button, MenuItem } from "@blueprintjs/core";

import { Specification } from "../../pages/Create/CreateTypes";

const SpecSelect = Select.ofType<Specification>();

const renderSpec: ItemRenderer<Specification> = (
  spec,
  { handleClick, modifiers }
) => {
  return (
    <MenuItem
      active={modifiers.active}
      key={spec.id}
      onClick={handleClick}
      text={spec.name}
    />
  );
};

interface ISpecSelectorProps {
  onSpecSelect: (spec: Specification) => void;

  specs: Array<Specification>;
  selectedSpec: Specification;
}

class SpecSelector extends React.Component<ISpecSelectorProps> {
  constructor(props) {
    super(props);
  }

  onSpecSelect = spec => {
    this.props.onSpecSelect(spec);
  };

  render() {
    return (
      <div>
        <SpecSelect
          onItemSelect={this.onSpecSelect}
          itemRenderer={renderSpec}
          itemPredicate={(q, x, i) => x.name.includes(q)}
          items={this.props.specs}
        >
          <Button
            rightIcon="caret-down"
            icon="list"
            intent="none"
            text={
              (this.props.selectedSpec && this.props.selectedSpec.name) ||
              "Select specification"
            }
          />
        </SpecSelect>
      </div>
    );
  }
}

export default SpecSelector;
