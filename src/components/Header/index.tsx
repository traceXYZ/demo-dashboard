import * as React from "react";
import {
  Navbar,
  NavbarGroup,
  NavbarDivider,
  NavbarHeading,
  Alignment,
  AnchorButton
} from "@blueprintjs/core";

class Header extends React.Component {
  render() {
    return (
      <Navbar>
        <NavbarGroup>
          <NavbarHeading>
            <a href="/">TraceXYZ</a>
          </NavbarHeading>
        </NavbarGroup>
        <NavbarGroup align={Alignment.LEFT}>
          <NavbarDivider />
          {/* <AnchorButton
            minimal
            intent="primary"
            icon="user"
            text="Register User"
            href="/createUser"
          /> */}
          <AnchorButton
            minimal
            intent="warning"
            icon="log-in"
            text="Login Users"
            href="/loginUsers"
          />
          <NavbarDivider />
          <AnchorButton
            minimal
            intent="danger"
            text="Editor"
            href="/editor"
            icon="edit"
          />
          <AnchorButton
            minimal
            intent="primary"
            icon="form"
            text="Specifications"
            href="/spec"
          />
          <AnchorButton
            minimal
            icon="add"
            text="Production"
            href="/create"
            intent="none"
          />
          <AnchorButton
            minimal
            icon="dollar"
            text="Buy &amp; Sell"
            href="/sales"
            intent="none"
          />
          <AnchorButton
            minimal
            icon="graph"
            text="Tracing"
            href="/trace"
            intent="success"
          />
          <AnchorButton
            minimal
            icon="list-detail-view"
            text="Watchlist"
            href="/watchlist"
            intent="success"
          />
        </NavbarGroup>
      </Navbar>
    );
  }
}

export default Header;
