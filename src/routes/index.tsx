import * as React from "react";
import { Route, Switch } from "react-router-dom";

import Header from "../components/Header";

import HomePage from "../pages/Home/Home";
import EditorPage from "../pages/Editor";
import SpecEditorPage from "../pages/SpecEditor";
import CreateUserPage from "../pages/CreateUser";
import TracePage from "../pages/Trace";
import MultiLoginPage from "../pages/MultiLogin";
import CreateItemPage from "../pages/Create";
import SalesPage from "../pages/Sales";
import WatchlistPage from "../pages/Watchlist";

export default (
  <>
    <Header />
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path="/createUser" component={CreateUserPage} />
      <Route path="/loginUsers" component={MultiLoginPage} />
      <Route path="/editor" component={EditorPage} />
      <Route path="/spec" component={SpecEditorPage} />
      <Route path="/create" component={CreateItemPage} />
      <Route path="/trace" component={TracePage} />
      <Route path="/sales" component={SalesPage} />
      <Route path="/watchlist" component={WatchlistPage} />
    </Switch>
  </>
);
