import apiConfigLocal from "../config/api.config";

async function post(path: string, body: string, token: string = null) {
  const headers = { "Content-Type": "application/json" };

  if (token) {
    headers["Authorization"] = `Bearer ${token}`;
  }

  let response = await fetch(`${apiConfigLocal.CLIENT_PATH}/${path}`, {
    body,
    method: "post",
    headers,
    mode: "cors"
  });

  const result = await response.text();
  return result;
}

async function get(path: string, token: string = null) {
  const headers = { "Content-Type": "application/json" };

  if (token) {
    headers["Authorization"] = `Bearer ${token}`;
  }

  let response = await fetch(`${apiConfigLocal.CLIENT_PATH}/${path}`, {
    method: "get",
    headers,
    mode: "cors"
  });

  const result = await response.text();
  return result;
}

export async function createUser(payload): Promise<string> {
  return post("register", payload);
}

export async function loginUser(payload): Promise<string> {
  return post("login", payload);
}

export async function addSpecification(
  json: string,
  token: string
): Promise<string> {
  return post("api/specification", json, token);
}

export async function invoke(json: string, token: string): Promise<string> {
  return post("api/invoke", json, token);
}

export async function query(json: string, token: string): Promise<string> {
  return post("api/query", json, token);
}

export async function fetchSpecifications(token: string) {
  return get("api/specifications", token);
}

export async function createItem(json: string, token: string) {
  return post("api/create", json, token);
}

export async function getOwnedItems(token: string) {
  return get("api/own", token);
}

export async function getHandledItems(token: string) {
  return get("api/handle", token);
}

export async function sell(json: string, token: string) {
  return post("api/sell", json, token);
}

export async function getPendingBuyItems(token: string) {
  return get("api/pending-buy", token);
}

export async function buy(json: string, token: string) {
  return post("api/buy", json, token);
}

export async function getPendingConfirmItems(token: string) {
  return get("api/pending-confirm", token);
}

export async function confirm(json: string, token: string) {
  return post("api/confirm", json, token);
}
