import { createStore, applyMiddleware, Store, AnyAction, Action } from "redux";
import createSagaMiddleware, { END, Task } from "redux-saga";
import loggerMiddleware from "redux-logger";

import rootReducer, { RootState } from "./reducer";

interface SagaEnabledStore<S = any, A extends Action<any> = AnyAction>
  extends Store<S, A> {
  runSaga: (saga: any /*Saga0, Saga1, ...*/) => Task;
  close: () => any;
}

export function configureStore(initialState?: RootState) {
  let sagaMiddleware = createSagaMiddleware();
  let middleware = applyMiddleware(sagaMiddleware, loggerMiddleware);

  const store = createStore(
    rootReducer,
    initialState,
    middleware
  ) as SagaEnabledStore<RootState>;

  store.runSaga = sagaMiddleware.run;
  store.close = () => store.dispatch(END);

  return store;
}
